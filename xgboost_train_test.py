from __future__ import print_function

#Import libraries:
import sys
import xgboost as xgb
import time
import pickle # library to save and load the trained model
import datetime as dt
from operator import itemgetter
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_curve, auc,recall_score,precision_score
from MLsrc.prepare_data_final import *
import matplotlib.pylab as plt
from matplotlib.pylab import rcParams
import random
from datetime import datetime
import pandas as pd
import numpy as np

###################################################################################################
###################################################################################################

def plot_train_test_error(model, X_train, y_train, X_test, y_test):

    eval_set = [(X_train, y_train), (X_test, y_test)]
    #model.train(X_train, y_train, eval_metric=["roc_auc"], eval_set=eval_set, verbose=True)
    # make predictions for test data
    y_pred = model.predict(X_test)
    predictions = [round(value) for value in y_pred]
    # evaluate predictions
    accuracy = accuracy_score(y_test, predictions)
    print("Accuracy: %.2f%%" % (accuracy * 100.0))
    # retrieve performance metrics
    results = model.evals_result()
    epochs = len(results['validation_0']['error'])
    x_axis = range(0, epochs)
    # plot log loss
    fig, ax = plt.subplots()
    ax.plot(x_axis, results['validation_0']['logloss'], label='Train')
    ax.plot(x_axis, results['validation_1']['logloss'], label='Test')
    ax.legend()
    plt.ylabel('Log Loss')
    plt.title('XGBoost Log Loss')
    plt.savefig("log_loss_xgboost_no1tr.pdf")
    plt.show()
    # plot classification error
    fig, ax = plt.subplots()
    ax.plot(x_axis, results['validation_0']['error'], label='Train')
    ax.plot(x_axis, results['validation_1']['error'], label='Test')
    ax.legend()
    plt.ylabel('Classification Error')
    plt.title('XGBoost Classification Error')
    plt.savefig("class_error_xgboost.pdf")
    
    plt.show()

######################################################################################################  
###################################################################################################### 
def save_xgb_model(xgb_):
    # save model to file
    pickle.dump(xgb_,open("hbar_xgb.pickle.dat", "wb"))
    
######################################################################################################  
######################################################################################################
def load_xgb_model(pickle_file_name):
    loaded_xgb = pickle.load(open(pickle_file_name, "rb"))
    
    return loaded_xgb
    
######################################################################################################  
###################################################################################################### 

def create_feature_map(features):
    outfile = open('xgb.fmap', 'w')
    for i, feat in enumerate(features):
        outfile.write('{0}\t{1}\tq\n'.format(i, feat))
    outfile.close()
    
######################################################################################################  
######################################################################################################  

def get_importance(gbm, features):
    create_feature_map(features)
    importance = gbm.get_fscore(fmap='xgb.fmap')
    importance = sorted(importance.items(), key=itemgetter(1), reverse=True)
    
    return importance
    
######################################################################################################  
######################################################################################################  

def get_features(train, test):
    trainval = list(train.columns.values)
    output = trainval
    
    return sorted(output)

######################################################################################################  
######################################################################################################

def plot_feature_imps(alg):

    # The feature importances. The higher, the more important the feature.
    # The importance of a feature is computed as the (normalized) total reduction
    # of the criterion brought by that feature. It is also known as the Gini importance
    
    # how useful or valuable each feature was in the construction of the boosted decision
    # trees within the model. The more an attribute is used to make key decisions with
    # decision trees, the higher its relative importance.
     
    # my description: how good is a split on that variable in matters of purity improvement  
     
    fig, ax = plt.subplots(figsize = (8,6))
    fig.subplots_adjust(hspace=0.0,wspace=0)
    fig.patch.set_alpha(0.0) 
              
    bluec = (36.0/255,75.0/255,166.0/255)
    #fig.tight_layout() 
    ax.set_ylabel('Feature Importance Score', fontsize = 15)
    #ax.legend(loc='best', fancybox=True, framealpha=0.5, fontsize = 15)
    plt.tight_layout()
    plt.gcf().subplots_adjust(bottom=0.25)
                          
    feat_imp = pd.Series(alg.booster().get_fscore()).sort_values(ascending=False)
    
    print(feat_imp)
    
    #print feat_imp.shape
    #feat_imp.reshape(9,2)
    
    #strArr = numpy.empty(9, dtype='string')
    
    #ax.hist(feat_imp.index, feat_imp.iloc[:], histtype = 'bar')
    feat_imp.plot(kind='bar', title='Feature Importances', color = bluec, fontsize = 15)
    
    plt.savefig('feature_imp.pdf')
    plt.show()

######################################################################################################
######################################################################################################

def run_single(nr_xgb, train, test, valid, predictors, target):

    plot_roc = False
    start_time = time.time()
    early_stopping_rounds = 20
    y_train = train[target]
    y_valid = valid[target]

    dtrain = xgb.DMatrix(train[predictors], y_train, missing=999)
    dvalid = xgb.DMatrix(valid[predictors], y_valid, missing=999)

    # TPE new new with regularisation 
    
    tim = datetime.now()
    randseed = tim.hour*10000+tim.minute*100+tim.second
    
    # Hyperopt estimated optimum
    params = {
        "objective": "binary:logistic",
        #"booster" : "gbtree",
        "eval_metric": "auc",
        "reg_alpha": 0.21876916567664978,
        "colsample_bytree": 0.7964006923073769,
        "learning_rate": 0.09204513386599818,
        "max_delta_step": 4.996698512573651,
        #"min_child_weight": 0.0,
        "n_estimators": 3773,
        "subsample": 0.9224434653576445,
        "reg_lambda": 0.09879847889762797,
        "max_depth": 20,
        "gamma": 0.04755589354076043,
        "silent": 1,
        "seed": randseed
    }
    
    # TRAINING
    watchlist = [(dtrain, 'train'), (dvalid, 'eval')]
    gbm = xgb.train(params, dtrain, 50000, evals=watchlist,  early_stopping_rounds=early_stopping_rounds, verbose_eval=False)

    # check validation sample
    print(" ")
    print("############################################################################")
    print(("Validating... ", gbm.best_iteration))

    check = gbm.predict(xgb.DMatrix(valid[predictors], missing=999), ntree_limit=gbm.best_iteration+1)
  
    #area under the precision-recall curve
    score = average_precision_score(valid[target].values, check)
    print('area under the precision-recall curve: {:.6f}'.format(score))

    check2 = check.round()
    score = precision_score(valid[target].values, check2)
    print('precision score: {:.6f}'.format(score))

    score = recall_score(valid[target].values, check2)
    print('recall score: {:.6f}'.format(score))
   
    print("############################################################################")
    print(" ")

    imp = get_importance(gbm, predictors)

    print(('Importance array: ', imp))
    print("############################################################################")
    print("Predict test set... ")

    test_prediction = gbm.predict(xgb.DMatrix(test[predictors], missing=999), ntree_limit=gbm.best_iteration+1)
    score = average_precision_score(test[target].values, test_prediction)

    print('area under the precision-recall curve test set: {:.6f}'.format(score))
      
    #Predict test set:
    dtest_p = test[test['Class'] == 1] 
    dtest_c = test[test['Class'] == 0] 
    
    print("len p set ", len(dtest_p))
    print("len c set ", len(dtest_c))

    dtest_p_predictions = gbm.predict(xgb.DMatrix(dtest_p[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)
    dtest_c_predictions = gbm.predict(xgb.DMatrix(dtest_c[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)

    print(" ")
    print(" ")
    print(np.min(dtest_c_predictions), np.max(dtest_c_predictions))
    print(np.min(dtest_p_predictions), np.max(dtest_p_predictions))
    print(" ")

    # if the max and min values of the test sets are not 0 and 1, then the test set was most likely not representative
    # of the train set this is a result of small data sets in combination with random sampling
    # different sampling methods could get rid of this problems!
    # in case of this, discard the run and re-run
    if np.min(dtest_c_predictions) > 0.01 or np.max(dtest_c_predictions) < 0.98 or np.min(dtest_p_predictions) > 0.01 or np.max(dtest_p_predictions) < 0.98:
        return -999, -999, -999, -999

    # write out scores of test set:      
    np.savetxt("./events_pred_cos_pbar_rounds/events_and_prediction_pbar_{}.dat".format(nr_xgb), dtest_p_predictions )    
    np.savetxt("./events_pred_cos_pbar_rounds/events_and_prediction_cosmic_{}.dat".format(nr_xgb), dtest_c_predictions)   

    # Compute micro-average ROC curve and ROC area
    if plot_roc:
   
        fpr, tpr, _ = roc_curve(valid[target].values, check)
        
        np.savetxt('roc_gr0_2.txt', np.c_[fpr, tpr])
        
        roc_auc = auc(fpr, tpr)
     
        plt.figure()
        lw = 2
        plt.plot(fpr, tpr, color='darkorange',
                 lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
        plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
        plt.xlim([-0.02, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC curve')
        plt.legend(loc="lower right")
        
        xgb.plot_importance(gbm)
        plt.show()

    print("############################################################################")

    print('Training time: {} minutes'.format(round((time.time() - start_time)/60, 2)))
    
    print("############################################################################")
    
        
    return test_prediction, imp, gbm.best_iteration+1, gbm


  
if __name__ == '__main__':
        
    rcParams['figure.figsize'] = 12, 4

    ######################################################################################################
    # Target variable: variable that is or should be the output.(binary 0 or 1 if you are classifying)
    #
    # Predictor variables: input data or the variables that is mapped to the target variable 
    #
    # score: Returns the mean accuracy on the given test data and labels
    #
    # effect of weighting the addition of new trees to a gradient boosting model, called shrinkage or the learning rate
    # That adding a learning rate is intended to slow down the adaptation of the model to the training data.
    # The learning rate parameter ([0,1]) in Gradient Boosting shrinks the contribution of
    # each new base model - typically a shallow tree - that is added in the series.
    #
    #
    # The idea of boosted decision trees is to first train a classifier using the training data
    # and to use, for the next training iteration, a modified training sample in which the
    # previously misclassified events are given a larger weight. This procedure is then iterated,
    # and finally the result of all the different classifiers obtained is averaged. The final
    # classifier is then a linear combination of the so-called base classifiers,
    #
    #
    # Different boosting algorithms, that is different prescriptions of how the event
    # weights are updated in each training step and how the various base classifiers are
    # weighted in the final linear combination, correspond to different loss functions
    # used in the stepwise minimisation 

    
    ######################################################################################################
    # LOADING DATA
    np.set_printoptions(threshold=sys.maxsize)
    pbardata = np.loadtxt("./training_data/all_pbars_data_2016_yangle.dat")
    cosmicdata = np.loadtxt("./training_data/all_cosmics_data_2016_halfbreak1_yangle.dat")
    hbardata_orig = np.loadtxt("./training_data/all_hbar_fi_runs_2016_triggercut.dat")

    tot_c = 282935.0        # half break 1... hodoscope+BGO trigger
    tot_c_time = 603659.0   # in seconds, half break 1... hodoscope+BGO trigger
    cosmic_rate = tot_c/tot_c_time

    print(len(cosmicdata)/tot_c_time)
    
    print("-----------------------------------------------------------------------------------------------")
    print("----------------------------------- START OF ANALYSIS -----------------------------------------")
    print("-----------------------------------------------------------------------------------------------")
    print("tot cosmics: all events recorded, events in cosmic data file", tot_c, len(cosmicdata))
    print("events in pbar file (before vertex cut, before timeinterval cut): ", len(pbardata))
    print("cosmic rate", cosmic_rate)

    samp = "custom"     # what sampling method to overcome imbalanced data?
    test_size = 0.33    # size of test sample
    seed = random.seed(datetime.now())           # random seed
           
    bool_cut_vertex = True      # cut on vertex ?
    track_cut = 0               # events with tracks > track_cut will be accepted
    BGO_cut = 0.0               # events with BGO energy deposit > BGO_cut will be accepted
    validbool = True            # validation sample?
    valid_size = 0.2            # size of validation sample?
    stratified_testing = True
    print("track cut:", track_cut)  
    target = 'Class'  
    
    cosmics_under_BGO_cut = len(cosmicdata[cosmicdata[:,3] < BGO_cut])

    hbardata, hbardata_orig = prepare_hbardata(hbardata_orig, track_cut, BGO_cut)

    #####################################################################################################    
    #####################################################################################################
    # xgboost style... for sklearn style with wrapper see below
        
    nr_of_xgboosts = int(argv[1])
    counter = 0
    fail_counter = 0

    while(counter < nr_of_xgboosts):
        print("############################################################### ", counter)              
        print("-----------------------------------------------------------------------------------------------")        
        train, test, valid, estimated_cosmics, cosmic_test_ratio, tot_p, tot_p_aftertrack = prepare_data(tot_c_time, pbardata, cosmicdata, samp, test_size, seed, bool_cut_vertex, track_cut, BGO_cut, validbool, valid_size, stratified_testing)
        
        print("len test cosmic/pbar", len(test[test[target]==0]), len(test[test[target]==1]))
        print("len(valid) cosmic/pbar", len(valid[valid[target]==0]),len(valid[valid[target]==1]))
        print("len train cosmic/pbar", len(train[train[target]==0]), len(train[train[target]==1]))
        
        c_under_bgocut_test = cosmics_under_BGO_cut*test_size

        print("-----------------------------------------------------------------------------------------------")
 
        predictors = [x for x in train.columns if x not in [target]] 

        print("predictors: ", predictors)

        start_time = dt.datetime.now()

        print(("Start time: ",start_time))
        print(("Building model.. ",dt.datetime.now()-start_time))
        print("-----------------------------------------------------------------------------------------------")
        print("----------------------------------------- XGB RUN ---------------------------------------------")
        
        estimated_cosmics = estimated_cosmics*test_size

        print("est cosmics", estimated_cosmics)

        preds, imp, num_boost_rounds, gbm = run_single(counter, train, test, valid, predictors, target)

        if imp == -999:      
            fail_counter += 1      
            continue

        print(dt.datetime.now()-start_time)

        print("-----------------------------------------------------------------------------------------------")
        print(" ")

        hbar_predictions = gbm.predict(xgb.DMatrix(hbardata[predictors],missing = 999), ntree_limit=num_boost_rounds)

        hbar_pred = hbar_predictions #hbar_predictions.astype(int)           
        txtfile = hbardata_orig      #[hbar_pred==1]
               
        print("Hbar data: number of cands: ", len(txtfile)) 
        print("len pred", len(hbar_pred))
                           
        xg_cands = open('./hbar_xgboost_cands_rounds/hbars_xgboost_round_{0}.dat'.format(counter), 'w')
        counter += 1
        
        
        for k, i in zip(hbar_pred, txtfile):
        
            xg_cands.write("%s " % k)  
            for j in i:
                xg_cands.write("%s " % j)

            xg_cands.write("\n") #"""
    
    print("Fail counter: ", fail_counter)
