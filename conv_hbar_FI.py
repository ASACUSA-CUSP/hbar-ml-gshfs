#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
from __future__ import print_function


import numpy as np

from sys import argv



import re
import math


pbarcolor = '#990001ff'
cosmiccolor = '#51989fff'

hbarcolor = 'mediumseagreen'

import matplotlib.pyplot as plt
import scipy.stats as stats
#################################################################################################################################


if __name__ == '__main__':


    fi_runs = np.loadtxt('fi_25e_runs_volt.dat')       
    filenames_hbar = [] #
     
    save_to_txt = True
    
    time_short = 6.0 # 6 secs
    
    runs = np.arange(0,int(argv[1]))
    
    for i in runs:
        name = "./hbar_xgboost_cands_rounds/hbars_xgboost_round_" + str(i) + ".dat"
        filenames_hbar.append(name)  
              
##############################################################################################################################    

    tot_data_hbar = []
    tot_data_hbar_norm = []
    
    print("saving data....")
    
    for i in filenames_hbar:        
        print(i)
        data = np.loadtxt(i)
        # add two columns with zeros:
        data = np.hstack((data, np.zeros((data.shape[0], 1))))
        data = np.hstack((data, np.zeros((data.shape[0], 1))))
        # write fi voltages into the columns
        for ev in data:
            for run in fi_runs:
                #print run[0], ev[0]+1
                if run[0] == int(ev[1] +1):
                    ev[32] = run[1]
                    ev[33] = run[2] #"""
                 
        
        tot_data_hbar.append(data)
        
        if save_to_txt == True:
            #np.savetxt(i[:-4] + "_FI.dat", data, delimiter=' ')
            np.savetxt(i[:-4] + "_FI.dat", data, delimiter=' ')
         
    ##################################
        
    tot_data_hbar = np.array([item for sublist in tot_data_hbar for item in sublist])
     
    """print "adding columns with voltages of FI..."

    for ev, ev_n in zip(tot_data_hbar, tot_data_hbar_norm):
        for run in fi_runs:
            #print run[0], ev[0]+1
            if run[0] == int(ev[1] +1):
                ev[32] = run[1]
                ev[33] = run[2] 
    
            if run[0] == int(ev_n[1] +1):
                ev_n[32] = run[1]
                ev_n[33] = run[2] #"""
    
    
    h5000s = tot_data_hbar#[tot_data_hbar[:,33]==-5000.0]    
    h5000s= h5000s[np.logical_and(h5000s[:,5] > 0, h5000s[:,6] > 0) ]
    h5000s = h5000s[np.absolute(h5000s[:,29]) < time_short]
          
    print("plotting...")
    
    fig, ax = plt.subplots()
    range1 = 0
    range2 = 1     
    r = np.array([range1,range2])

    histc, edgesc = np.histogram(a=h5000s[:,0], bins=60, range=r)   
    bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.        
    norm = 1.0*sum(np.diff(edgesc)*histc)
    ax.errorbar(bincentersc, histc/norm, yerr= np.sqrt(histc)/norm, label = "score")#
   
    ax.legend() 
    ax.set_ylim(0.01,)
    ax.set_yscale('log')
    ax.set_xlim(range1,range2)  #"""

    plt.savefig("histo_scores_hbar_data.pdf")
    
    plt.show()
    
    exit(0)
    
    ######################################################################################################################################################################
    ######################################################################################################################################################################            
    ######################################################################################################################################################################
    ######################################################################################################################################################################
    fig, ax = plt.subplots()
    range1 = 0
    range2 = 1     
    r = np.array([range1,range2])
    

    histc, edgesc = np.histogram(a=tot_data_cos, bins=60, range=r)   
    bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.    
    
    norm = 1.0*sum(np.diff(edgesc)*histc)
    ax.errorbar(bincentersc, histc/norm, yerr= np.sqrt(histc)/norm)#
    
    
    cos_rej = len(tot_data_cos[tot_data_cos <= 0.4])/len(tot_data_cos)
    print(cos_rej)
    print((1 - cos_rej)*0.4687)
    
    #####################
    
    histc, edgesc = np.histogram(a=tot_data_pbar, bins=60, range=r)   
    bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.    
    
    norm = 1.0*sum(np.diff(edgesc)*histc)
    ax.errorbar(bincentersc, histc/norm, yerr= np.sqrt(histc)/norm)#
        
    print(len(tot_data_pbar[tot_data_pbar > 0.4])/len(tot_data_pbar))    
  
    ax.legend() 
    ax.set_ylim(0.01,)
    ax.set_yscale('log')
    ax.set_xlim(range1,range2)
    #plt.show()    #"""   
            
    ######################################################################################################################################################################
    ######################################################################################################################################################################            
    
    score_cut = np.arange(0,1,0.01)
    
    cosrej_scan = []
    pbareff_scan = []
    
    for i in score_cut:
        #print i 
        cosrej_scan.append(len(tot_data_cos[tot_data_cos <= i])/len(tot_data_cos))    
        pbareff_scan.append(len(tot_data_pbar[tot_data_pbar > i])/len(tot_data_pbar))                
            
    fig2, ax2 = plt.subplots()
    
    
    #plt.plot(score_cut, cosrej_scan)
    #plt.plot(score_cut, pbareff_scan)             
    plt.plot(cosrej_scan, pbareff_scan)        
    ax2.set_xlabel('cos rej')   
    ax2.set_ylabel('pbar eff')
    
    fig3, ax3 = plt.subplots()
    
    
    plt.plot(score_cut, cosrej_scan, label = 'cos rej')
    plt.plot(score_cut, pbareff_scan, label = 'pbar eff')
    ax3.set_xlabel('cut on score')
    plt.legend()           
    #plt.plot(cosrej_scan, pbareff_scan)        
                  
    plt.show()   #""" 
    
    exit(0)   

    ######################################################################################################################################################################
    ######################################################################################################################################################################            
    time_short = 6
    h5000s = tot_data_hbar[tot_data_hbar[:,33]==-5000.0]
    
    h5000s= h5000s[np.logical_and(h5000s[:,5] > 0, h5000s[:,6] > 0) ]
    #print h5000s[:,29]
    
    h5000s = h5000s[np.absolute(h5000s[:,29]) < time_short]
    
    #print 
    save_pval = []
    
    for i in score_cut:
        curr_hs = len(h5000s[h5000s[:,0] > i])        
        cos_rej = len(tot_data_cos[tot_data_cos <= i])/len(tot_data_cos)
        fp_rate =  (1.0 - cos_rej)*0.4687  
        #print fp_rate
        
        fake_h = fp_rate*time_short*(43.0)#+31+16+24)  
    
        p_val_h = stats.poisson.sf(curr_hs, fake_h) 
        
        
        sigmas_h = stats.norm.ppf(1 - p_val_h)
        
        print(i, "  ", cos_rej, "   ", curr_hs, fake_h, "diff:", curr_hs - fake_h, "   ", p_val_h, sigmas_h)
        
        
        save_pval.append(sigmas_h)
        
        #print "pval ", p_val_h
        #   
            


    fig4, ax4 = plt.subplots()
    
    
    #plt.plot(score_cut, cosrej_scan)
    #plt.plot(score_cut, pbareff_scan)  
    ax4.set_yscale('log')           
    plt.plot(score_cut, save_pval)  
          
                        
    plt.show()            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
    
        #thefile.close()   

