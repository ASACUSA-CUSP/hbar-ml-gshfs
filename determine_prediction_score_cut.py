#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
from __future__ import print_function


import numpy as np

from sys           import argv

import matplotlib
import sys

import re
import math

from scipy.stats import gaussian_kde


pbarcolor = '#990001'
cosmiccolor = '#51989f'


hbarcolor = 'mediumseagreen'

import matplotlib.pyplot as plt
import scipy.stats as stats
#################################################################################################################################
#################################################################################################################################
def calc_length_run(mix_runs_):

    times_file = np.loadtxt("runnr_times_evts_rate.dat")
    
    times = []
    
    for i in mix_runs_:
    
        curr_times = times_file[times_file[:,0]==i]
        if len(curr_times) > 0:
            times.append(curr_times)
        
        
    times = (np.array(times)).reshape(len(times), 4)
    
    print(times.shape)
    
    print("mean rate: ", np.mean(times[:,3]))
    
    
    tot_time = np.sum(times[:,1])
    
    
    return times[:,1], tot_time
        
    
#################################################################################################################################
#################################################################################################################################
def plot_eff_vs_rej(tot_data_cosmic_, tot_data_pbar_):
    print(" ")
    print("plotting pbar efficiency versus cosmic rejection ... ")
    fig, ax = plt.subplots()
    range1 = 0
    range2 = 1     
    r = np.array([range1,range2])   

    histc, edgesc = np.histogram(a=tot_data_cosmic_, bins=60, range=r)   
    bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.    
    
    norm = 1.0*sum(np.diff(edgesc)*histc)
    ax.errorbar(bincentersc, histc/norm, yerr= np.sqrt(histc)/norm, color = cosmiccolor, label = "cosmics",linewidth= 1.5)#
    
    range1 = 0
    range2 = 1
    
    
    #cos_rej = len(tot_data_cosmic[tot_data_cosmic <= 0.4])/len(tot_data_cosmic)
    #print cos_rej
    #print (1 - cos_rej)*0.4687
    
    #####################
    
    histc, edgesc = np.histogram(a=tot_data_pbar_, bins=60, range=r)   
    bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.    
    
    norm = 1.0*sum(np.diff(edgesc)*histc)
    ax.errorbar(bincentersc, histc/norm, yerr= np.sqrt(histc)/norm, color = pbarcolor, label = "antiprotons",linewidth= 1.5)#
        
    #print len(tot_data_pbar[tot_data_pbar > 0.4])/len(tot_data_pbar)    
  
    ax.legend() 
    ax.set_ylim(0.01,)
    ax.set_ylabel("norm. counts", fontsize = 12)
    ax.set_xlabel("score", fontsize = 12)
    ax.set_yscale('log')
    ax.set_xlim(range1,range2)
    
    plt.savefig("xgb_histo_scores_pbar_cosmic.pdf")
    
    plt.show()    #"""   
#################################################################################################################################
#################################################################################################################################            
def plot_distr_hbar_pred_score(all_hbars_):
    print(" ")
    print("plotting distribution of Hbar prediction scores ... ")
    fig, ax = plt.subplots()
    range1 = 0
    range2 = 1     
    r = np.array([range1,range2])

    #histc, edgesc = np.histogram(a=hbars_allfi[:,1], bins=60, range=r)   
    #bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.        
    #norm = 1.0*sum(np.diff(edgesc)*histc)
    #ax.errorbar(bincentersc, histc/norm, yerr= np.sqrt(histc)/norm, label = "hbar score")#

    histc, edgesc = np.histogram(a=all_hbars_[:,2], bins=40, range=r)   
    bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.        
    norm = 1.0*sum(np.diff(edgesc)*histc)
    ax.errorbar(bincentersc, histc/norm, yerr= np.sqrt(histc)/norm, label = "$\overline{H}$, $M_s$", color = hbarcolor, linewidth= 1.5)#

    ax.legend() 
    ax.set_ylabel("norm. counts", fontsize = 13)
    ax.set_xlabel("$M_s$ of score", fontsize = 13)
    ax.set_ylim(0.01,)
    ax.set_yscale('log')
    ax.set_xlim(range1,range2)  

    plt.savefig("xgb_histo_scores_median_hbar.pdf")
    
    plt.show()
    
#################################################################################################################################
#################################################################################################################################
def plot_eff_rej_vs_score(tot_data_cosmic_, tot_data_pbar_):
    print(" ")
    print("plotting pbar efficiency and cosmic rejection versus cut on score ... ")
    fig, ax = plt.subplots()
    range1 = 0
    range2 = 1     
    r = np.array([range1,range2])

    
    score_cut = np.arange(0,1,0.001)
    
    cosrej_scan = []
    pbareff_scan = []
    
    for i in score_cut:
        #print i 
        cosrej_scan.append(len(tot_data_cosmic_[tot_data_cosmic_ <= i])/len(tot_data_cosmic_))    
        pbareff_scan.append(len(tot_data_pbar_[tot_data_pbar_ > i])/len(tot_data_pbar_))                
            
    fig2, ax2 = plt.subplots()
    plt.tick_params(axis='both', which='major', labelsize=15)
    
    #plt.plot(score_cut, cosrej_scan)
    #plt.plot(score_cut, pbareff_scan)             
    plt.plot(pbareff_scan,np.array(cosrej_scan), color = 'darkslateblue', linewidth= 1.5)        
    ax2.set_ylabel('$\\varepsilon_c$', fontsize = 22)
    ax2.set_xlabel('$\\varepsilon_{\overline{p}}$', fontsize = 22)
    plt.tight_layout()
    plt.savefig("xgb_cosrej_vs_pbareff.pdf")
    
    
    
    
    fig3, ax3 = plt.subplots()
    plt.tick_params(axis='both', which='major', labelsize=15)
    
    plt.plot(score_cut, cosrej_scan, label = '$\\varepsilon_c$', color = cosmiccolor, linewidth= 1.5)
    plt.plot(score_cut, pbareff_scan, label = '$\\varepsilon_{\overline{p}}$', color = pbarcolor,linewidth= 1.5)
    ax3.set_xlabel('$s_{thres}$', fontsize = 22)
    ax3.set_ylabel('$\\varepsilon$', fontsize = 22)
    plt.tight_layout()
    plt.legend()           
    #plt.plot(cosrej_scan, pbareff_scan)
            
    plt.savefig("xgb_score_cut_vs_pbareff_cosrej.pdf") 

     
                  
    plt.show()  
    
#################################################################################################################################
#################################################################################################################################
#################################################################################################################################
#################################################################################################################################
#################################################################################################################################
#################################################################################################################################

if __name__ == '__main__':


    fi_runs = np.loadtxt('fi_25e_runs_volt.dat') 
    #arocs_test_set = np.loadtxt("aroc_write_out_test.dat")    
    #print "Aroc of test set: ", np.mean(arocs_test_set), np.std(arocs_test_set)

    filenames_pbar = []
    filenames_cosmic = []

    plot_distr = True

    #time_short = 6.0 # 6 secs
    estimated_cosmics = 7.9761685 # per pbat test set 
    
    final_score_cut = 0.431  # for average cosmic rej and pbar eff
    
    print("###########################################################################")
    print(" ")
    print("cut on prediction score: ", final_score_cut)
    print(" ")
    print("reading cosmic, pbar and hbar data ... ")
    cos_rate = 0.4687
    nr_of_rounds = int(argv[1])
        
    runs = np.arange(0,nr_of_rounds)
    
    for i in runs:          
        name = "./events_pred_cos_pbar_rounds/events_and_prediction_cosmic_" + str(i) + ".dat"
        filenames_cosmic.append(name)
        
        name = "./events_pred_cos_pbar_rounds/events_and_prediction_pbar_" + str(i) + ".dat"
        filenames_pbar.append(name)
        
    ##############################################################################################################################    
 
    tot_data_cosmic = []
    tot_data_pbar = []
    tot_data_hbar = np.loadtxt("xgb_events_cands_mean_score.dat")
    #tot_data_hbar = np.loadtxt("xgb_events_cands_tpe_new0_mean_score_500_std.dat")
    # reading mode data: 
    #modes = np.loadtxt("modes_of_score_dists_hbar.dat") 
    #structure of mode dat file: 
    #myfile.write("%s " % cusp)               0
    #myfile.write("%s " % midas)              1
    #myfile.write("%s " % evt)                2
    #myfile.write("%s " % modee)              3
    #myfile.write("%s " % np.mean(scores))    4
    #myfile.write("%s " % np.median(scores))  5
    #myfile.write("%s " % sigma)              6    
    
    
    ########################################################################
    
    # cuts. mixing run events that we want to look at:
    temp = tot_data_hbar#[(tot_data_hbar_norm[:,1]) > 0.395]
    temp = temp[np.logical_and(temp[:,33] > 0, temp[:,32] > 0) ]    # trigger cut
    #temp = temp[np.absolute(temp[:,31]) < time_short]
    temp5000 = temp[temp[:,35]==-5000]   
    temp400 = temp[temp[:,35]==-400]
    temp70 = temp[temp[:,35]==-70]
    temp40 = temp[temp[:,35]==-40]
    
    print("total number of events, after trigger cuts, only q-dist. runs: ", len(temp5000)  +len(temp400)  +len(temp70)  +len(temp40)) 
    
    hbars_allfi = np.vstack((temp5000, temp400, temp70, temp40))
    
    hbars_allfi_cut = hbars_allfi[hbars_allfi[:,2]>final_score_cut]
    np.savetxt("all_hbar_cands_after_xgb_cut.dat", hbars_allfi_cut)
    ######################################################################################################################################################################
    ######################################################################################################################################################################            
        
    print(" ")
    print("calculating cosmic rejection and pbar efficiency for cut: ",  final_score_cut)
    mean_cos_rej = []
    mean_pbar_eff = []
    
    for i in filenames_cosmic:        
        data = np.loadtxt(i)
        mean_cos_rej.append(len(data[data<=final_score_cut])/(len(data)))
        tot_data_cosmic.append(data)        

    for i in filenames_pbar:  
        data = np.loadtxt(i)         
        mean_pbar_eff.append(len(data[data>final_score_cut])/(len(data) - estimated_cosmics))    
        tot_data_pbar.append(data)                      
    ##################################
    
    print("mean cosmic rej.: ", np.mean(np.array(mean_cos_rej)), "+/-", np.std(np.array(mean_cos_rej)))
    print("mean fp rate:     ", np.mean((1 - np.array(mean_cos_rej))*cos_rate), "+/-", np.std((1 - np.array(mean_cos_rej))*cos_rate))
    print("mean pbar eff.:   ", np.mean(np.array(mean_pbar_eff)), "+/-", np.std(np.array(mean_pbar_eff)))
    
    tot_data_cosmic = np.array([item for sublist in tot_data_cosmic for item in sublist])  
    tot_data_pbar = np.array([item for sublist in tot_data_pbar for item in sublist])  
            
    ######################################################################################################################################################################
    ######################################################################################################################################################################            
    if plot_distr == True:
        print("plotting... ")
            
        plot_distr_hbar_pred_score(hbars_allfi)
        
        plot_eff_vs_rej(tot_data_cosmic, tot_data_pbar)
        
        plot_eff_rej_vs_score(tot_data_cosmic, tot_data_pbar)
    
    ######################################################################################################################################################################
    ######################################################################################################################################################################            
    ######################################################################################################################################################################
    ######################################################################################################################################################################            
    time_short = 6
    
    score_cut = np.arange(0.22,0.9,0.01)
    
    h5000s = hbars_allfi#[hbars_allfi[:,35]==-5000.0]
    
    print(h5000s.shape)
    
    #h5000s= h5000s[np.logical_and(h5000s[:,32] > 0, h5000s[:,31] > 0) ]
    #print h5000s[:,29]
    
    #print h5000s.shape
    
    h5000s = h5000s[np.absolute(h5000s[:,32]) < time_short]
    #print h5000s.shape
   
    
    #print 
    save_pval = []
    save_sigs = []
    save_diff = []
    
    for i in score_cut:
        curr_hs = len(h5000s[h5000s[:,2] > i])     
           
        cos_rej = len(tot_data_cosmic[tot_data_cosmic <= i])/len(tot_data_cosmic)
        pbar_eff = len(tot_data_pbar[tot_data_pbar > i])/(len(tot_data_pbar) - estimated_cosmics*nr_of_rounds)
        
        fp_rate =  (1.0 - cos_rej)*cos_rate
        #print fp_rate
        
        fake_h = fp_rate*time_short*(43.0+31+16+24)  
    
        p_val_h = stats.poisson.sf(curr_hs, fake_h) 
        
        
        sigmas_h = stats.norm.ppf(1.0 - p_val_h)
        
        #print i, "  ", cos_rej, fp_rate, pbar_eff, "   ", curr_hs, fake_h, "diff:", curr_hs - fake_h, "   ", p_val_h, sigmas_h
        
        
        save_pval.append(p_val_h)
        save_sigs.append(sigmas_h)
        save_diff.append(curr_hs - fake_h)
        
        #print "pval ", p_val_h
        #   
            


    fig4, ax4 = plt.subplots(1, 3, figsize = (14,4)) 
    
    ax4[0].set_ylabel("p val") 
    ax4[0].set_yscale('log')           
    ax4[0].plot(score_cut, save_pval)  

    ax4[1].set_ylabel("sigma")     
    #ax4[1].set_yscale('log')           
    ax4[1].plot(score_cut, save_sigs) 
     
    ax4[2].set_ylabel("diff") 
    #ax4[2].set_yscale('log')           
    ax4[2].plot(score_cut, save_diff)      
          
                        
    plt.show()  
