from __future__ import print_function
from __future__ import division
from sklearn.metrics import average_precision_score

from sklearn.metrics import roc_curve, auc,recall_score,precision_score
from sklearn import metrics 
from sklearn.metrics import roc_auc_score


import warnings
with warnings.catch_warnings():
    warnings.filterwarnings("ignore",category=DeprecationWarning)
    import math
    import pandas as pd
    import numpy as np
    from datetime import datetime
    from scipy.stats import norm
    import xgboost as xgb
    from sklearn.preprocessing import StandardScaler
    #from sklearn.cross_validation import cross_val_score, cross_val_predict
    from sklearn.feature_extraction.text import CountVectorizer
    from sklearn.svm import SVR, SVC
    import matplotlib.pyplot as plt
    import scipy.interpolate
    from itertools import product, chain
    from sklearn.metrics import make_scorer, mean_squared_error

    from bayes_opt import BayesianOptimization
    
    
    from sklearn.preprocessing import LabelEncoder
    from sklearn.preprocessing import MinMaxScaler
    
    #import tqdm
    

from MLsrc.prepare_data_final import *
####################################################################################
#
#  The author of Bayesian Optimization is Fernando https://libraries.io/github/fmfn
#  For installing bayes_opt, see https://github.com/fmfn/BayesianOptimization
#
####################################################################################
##########################################################################################################
##########################################################################################################   
def timer(start_time=None):
    if not start_time:
        start_time = datetime.now()
        return start_time
    elif start_time:
        tmin, tsec = divmod((datetime.now() - start_time).total_seconds(), 60)
        print(" Time taken: %i minutes and %s seconds." % (tmin, round(tsec,2)))
##########################################################################################################

def XGbcv(n_estimators, eta, max_depth, gamma, min_child_weight, max_delta_step, subsample, colsample_bytree):

    global METRICbest
    global ITERbest
    #global log_file

    paramt = {
              'booster' : 'gbtree',
              'n_estimators' : n_estimators,
              'max_depth' : max_depth.astype(int),
              'gamma' : gamma,
              'eta' : eta,
              'objective': 'binary:logistic',
              'nthread' : 10,
              'silent' : True,
              #'eval_metric': 'logloss',
              'eval_metric': 'auc',
              'subsample' : subsample,
              'colsample_bytree' : colsample_bytree,
              'min_child_weight' : min_child_weight,
              'max_delta_step' : max_delta_step.astype(int),
              'seed' : 1001
              }

    folds = 5
    
    print("\n Search parameters (%d-fold validation):\n %s" % (folds, paramt), file=log_file )

    xgbr = xgb.cv(
           paramt,
           dtrain,
           num_boost_round = 100000,
           stratified = True,
           nfold = folds,
           verbose_eval = False,
           early_stopping_rounds = 50,
           #metrics = "logloss"#,
           metrics = "auc"#,
           #show_stdv = True
          )



    cv_score = xgbr['test-auc-mean'].iloc[-1]
    train_score = xgbr['train-auc-mean'].iloc[-1]

    print(' Stopped after %d iterations with train-auc = %f val-auc = %f ( diff = %f ) train-gini = %f val-gini = %f' 
    % ( len(xgbr), train_score, cv_score, (train_score - cv_score), (train_score*2-1),(cv_score*2-1)), file = log_file )

    #cv_score = xgbr['test-logloss-mean'].iloc[-1]
    #cv_score = xgbr['test-auc-mean'].iloc[-1]
    
    
    
    #cv_score = xgbr['auc'].iloc[-1]
    if ( cv_score > METRICbest ):
        METRICbest = cv_score
        ITERbest = len(xgbr)

    #return (-1.0 * cv_score)  # needs to be negative cos bayes maximizes, but we want to minimize logloss # """
    
    return (1.0 * cv_score)  # for auc
    #return (2.0 * cv_score) - 1.0 # gini from auc
##########################################################################################################

def scale_data(X, scaler=None):
    if not scaler:
        scaler = MinMaxScaler(feature_range=(-1, 1))
        scaler.fit(X)
    X = scaler.transform(X)
    return X, scaler
##########################################################################################################   

if __name__ == "__main__":

    folds = 5
    METRICbest = -1.0
    ITERbest = 0
    
    

  
    np.set_printoptions(threshold=np.nan)
    
    #cosmicdata = np.loadtxt('./training_data/cosmics_more_angles_1tracktoo_mergemore.dat') #np.loadtxt(argv[1])
    #pbardata = np.loadtxt('./training_data/pbars_more_angles_1tracktoo_mergemore.dat') #np.loadtxt(argv[2])
    pbardata = np.loadtxt("./training_data/all_pbars_data_2016_yangle.dat")
    cosmicdata = np.loadtxt("./training_data/all_cosmics_data_2016_halfbreak1_yangle.dat")
    #tot_c = 314776.0 # half break 2... BGO trigger
    #tot_c_time = 197074.0 # half second break
    tot_c = 282935.0 # half break 1... hodoscope+BGO trigger
    tot_c_time = 603659.0 # in seconds, half break 1... hodoscope+BGO trigger
    cosmic_rate = tot_c/tot_c_time
    #hbardata_orig = np.loadtxt('./all_events_cusp_eventnr_mergemore.dat') # all events in the low positron density mixing runs
    #algorithm = argv[1]
    
    #samp = "tomek"
    test_size = 0.33
    seed = 7    
    validbool = False
    valid_size = 0.0
    stratified_testing = False
    
    bool_cut_vertex = True # cut on vertex ?
    track_cut = 0
    
    
    sample_methods = sampling_arr = ["SMOTE"] #, "SMOTEENN", "SMOTETomek", "over_random", "tomek", "under_random"]
    
    
    runs = [1]
    #######################################################################################################
    #######################################################################################################
    #######################################################################################################
    
    
    
    BO_res_file = open('bayes_opt_results_hodortrigger_trackcut1_SMOTE_smilx0.dat', 'w')
    
    
    
    
    for nrrun in runs:
        BO_res_file.write("%s " % nrrun) 
        
    
        for samp in sample_methods:
        
        
            BO_res_file.write("%s " % samp)
        
            print ("----------------------------------------------------------------------------------------------------------------------------------------------------------------")
            print ("------------------------------------------------------------------------------")
            print ("method: ", samp)
            print ("------------------------------------------------------------------------------")
            
            train, test, estimated_cosmics, cosmic_test_ratio = prepare_data(tot_c_time,
                                                                                 pbardata,
                                                                                 cosmicdata,
                                                                                 samp,
                                                                                 test_size,
                                                                                 seed,
                                                                                 bool_cut_vertex,
                                                                                 track_cut,
                                                                                 validbool,
                                                                                 valid_size,
                                                                                 stratified_testing)
      
            
            log_file = open('./bayes_logs_smilx0/BAYESOPT-AUC-5fold-XGB-run-{0}-noscaler_{1}_hodortrigger_trackcut1.log'.format(nrrun, samp), 'a')

            
            
            
            
              #hbardata = prepare_hbardata(hbardata_orig)
           
            target = 'Class'  
            
            predictors = [x for x in train.columns if x not in [target]] 
            
            
            #for column in predictors:
            #    le = LabelEncoder()
            #    train[column] = le.fit_transform(train[column])
            
            
            
            #print "predictors: ", predictors
            X_train = train[predictors]
            #X_train, scaler = scale_data(X_train1)
            y_train = train[target].reshape(len(train[target]),1)
            
            X_test = test[predictors]
            #X_test, scaler = scale_data(X_test1)
            y_test = test[target].reshape(len(test[target]),1)
            
            ids = test['Class']



        ##############################################################################################



            dtrain = xgb.DMatrix(X_train, label=y_train, missing = 999)
            dtest = xgb.DMatrix(X_test, label = y_test, missing = 999)
           
            print("\n Train Set Matrix Dimensions: %d x %d" % (X_train.shape[0], X_train.shape[1]))
            print("\n Test Set Matrix Dimensions: %d x %d\n" % (X_test.shape[0], X_test.shape[1]))

            start_time = timer(None)
            print("# Global Optimization Search for XGboost Parameters")
            

            XGbBO = BayesianOptimization(XGbcv, {'eta': (0.01, 1),
                                             'n_estimators': (300, 6000),
                                             'max_depth': (1, 40),
                                             'gamma': (0.00001, 10.0),
                                             'min_child_weight': (0, 50),
                                             'max_delta_step': (0.01, 5),
                                             'subsample': (0.01, 1.0),
                                             'colsample_bytree' :(0.15, 1.0)
                                            })
                                                



            XGbBO.maximize(init_points=20, n_iter=40, acq="ei", xi=0.0)
            
            print("-" * 53)
            timer(start_time)

            best_METRIC = round((1.0 * XGbBO.res['max']['max_val']), 6)
            max_depth = XGbBO.res['max']['max_params']['max_depth']
            n_estimators = XGbBO.res['max']['max_params']['n_estimators']
            eta = XGbBO.res['max']['max_params']['eta']
            gamma = XGbBO.res['max']['max_params']['gamma']
            min_child_weight = XGbBO.res['max']['max_params']['min_child_weight']
            max_delta_step = XGbBO.res['max']['max_params']['max_delta_step']
            subsample = XGbBO.res['max']['max_params']['subsample']
            colsample_bytree = XGbBO.res['max']['max_params']['colsample_bytree']

            print("\n Best METRIC value: %f" % best_METRIC)
            print(" Best XGboost parameters:")
            print(" max_depth=%d gamma=%f min_child_weight=%f max_delta_step=%d subsample=%f colsample_bytree=%f " % (int(max_depth), gamma, min_child_weight, int(max_delta_step), subsample, colsample_bytree))
            print(" n_estimators=%d eta=%f" % (int(n_estimators), eta))
            BO_res_file.write("%s " % best_METRIC)
            


            start_time = timer(None)
            print("\n# Making Prediction")

            paramt = {
                      'booster' : 'gbtree',
                      'n_estimators' : n_estimators,
                      'max_depth' : max_depth.astype(int),
                      'gamma' : gamma,
                      'eta' : eta,
                      'objective': 'binary:logistic',
                      'nthread' : 3,
                      'silent' : True,
                      #'eval_metric': 'logloss',
                      'eval_metric': 'auc',
                      'subsample' : subsample,
                      'colsample_bytree' : colsample_bytree,
                      'min_child_weight' : min_child_weight,
                      'max_delta_step' : max_delta_step.astype(int),
                      'seed' : 1001
                      }

            
              
            print('-'*52)
            print('Final Results')
            print('Maximum XGBOOST value: %f' % XGbBO.res['max']['max_val'])
            print('Best XGBOOST parameters: ', XGbBO.res['max']['max_params'])
            print('-'*130, file=log_file)
            print('Final Result:', file=log_file)
            print('Maximum XGBOOST value: %f' % XGbBO.res['max']['max_val'], file=log_file)
            print('Best XGBOOST parameters: ', XGbBO.res['max']['max_params'], file=log_file)
            log_file.flush()
            log_file.close()
                        
            

            history_df = pd.DataFrame(XGbBO.res['all']['params'])
            history_df2 = pd.DataFrame(XGbBO.res['all']['values'])
            history_df = pd.concat((history_df, history_df2), axis=1)
            history_df.rename(columns = { 0 : 'gini'}, inplace=True)
            history_df['AUC'] = ( history_df['gini'] + 1 ) / 2
            history_df.to_csv('./bayes_logs_smilx0/BAYESOPT-AUC-5fold-XGB-run-{0}-noscaler_{1}_hodortrigger_trackcut1.csv'.format(nrrun, samp))
            
            
            print("Training starting now ... xgb_bayesresult")

            xgb_bayesresult = xgb.train(paramt, dtrain, num_boost_round=int(ITERbest*(1+(1/folds))))

            print (" ")
            print ("############################################################################")
            
            print("Validating... xgb_bayesresult")
            
            gbm = xgb_bayesresult
            
            train_prediction = gbm.predict(xgb.DMatrix(train[predictors], missing = 999), ntree_limit=gbm.best_iteration+1)
            train_prediction2 = train_prediction.round()
            
            train_p = train[train['Class'] == 1] 
            train_c = train[train['Class'] == 0] 
            train_p_prediction_orig = gbm.predict(xgb.DMatrix(train_p[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)
            train_p_prediction2 = train_p_prediction_orig.round()
            train_c_prediction_orig = gbm.predict(xgb.DMatrix(train_c[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)
            train_c_prediction2 = train_c_prediction_orig.round()
                
            ##############################################################################################################################
            #OVERALL
            ##############################################################################################################################
            
            print ("############################################################################")
            print ("\nModel Report --- train set:")
            
            accuracy_score = metrics.accuracy_score(train['Class'].values, train_prediction2)
            roc_auc_score = metrics.roc_auc_score(train['Class'].values, train_prediction2)            
            average_precision_score = metrics.average_precision_score(train['Class'].values, train_prediction)           
            precision_score = metrics.precision_score(train['Class'].values, train_prediction2)
            recall_score = metrics.recall_score(train['Class'].values, train_prediction2)
            f1_score = metrics.f1_score(train['Class'].values, train_prediction2, average='weighted')
            log_loss = metrics.log_loss(train['Class'].values, train_prediction2)
            hamming_loss = metrics.hamming_loss(train['Class'].values, train_prediction2)
            
                       
            print ("Accuracy : %.4g" % accuracy_score)
            print ("AUC Score (Train): %f" % roc_auc_score)
            
            BO_res_file.write("%s " % accuracy_score)
            BO_res_file.write("%s " % roc_auc_score)
            BO_res_file.write("%s " % average_precision_score)
            BO_res_file.write("%s " % precision_score)
            BO_res_file.write("%s " % recall_score)
            BO_res_file.write("%s " % f1_score)
            BO_res_file.write("%s " % log_loss)
            BO_res_file.write("%s " % hamming_loss)
            
            ##############################################################################################################################
            #COSMICS
            ##############################################################################################################################
            
            accuracy_score = metrics.accuracy_score(train_c['Class'].values, train_c_prediction2)
            #roc_auc_score = metrics.roc_auc_score(train_c['Class'].values, train_c_prediction2)            
            average_precision_score = metrics.average_precision_score(train_c['Class'].values, train_c_prediction_orig)           
            precision_score = metrics.precision_score(train_c['Class'].values, train_c_prediction2)
            recall_score = metrics.recall_score(train_c['Class'].values, train_c_prediction2)
            f1_score = metrics.f1_score(train_c['Class'].values, train_c_prediction2, average='weighted')
            #log_loss = metrics.log_loss(train_c['Class'].values, train_c_prediction2)
            #hamming_loss = metrics.hamming_loss(train_c['Class'].values, train_c_prediction2)
                        
            print ("Accuracy : %.4g" % accuracy_score)
            print ("AUC Score (Train): %f" % roc_auc_score)
            
            BO_res_file.write("%s " % accuracy_score)
            #BO_res_file.write("%s " % roc_auc_score)
            BO_res_file.write("%s " % average_precision_score)
            BO_res_file.write("%s " % precision_score)
            BO_res_file.write("%s " % recall_score)
            BO_res_file.write("%s " % f1_score)
            #BO_res_file.write("%s " % log_loss)
            #BO_res_file.write("%s " % hamming_loss)
            ######################################################
                      
            ##############################################################################################################################
            #PBARS
            ##############################################################################################################################
            
            accuracy_score = metrics.accuracy_score(train_p['Class'].values, train_p_prediction2)
            #roc_auc_score = metrics.roc_auc_score(train_p['Class'].values, train_p_prediction2)     # not defined for only 1 class        
            average_precision_score = metrics.average_precision_score(train_p['Class'].values, train_p_prediction_orig)           
            precision_score = metrics.precision_score(train_p['Class'].values, train_p_prediction2)
            recall_score = metrics.recall_score(train_p['Class'].values, train_p_prediction2)
            f1_score = metrics.f1_score(train_p['Class'].values, train_p_prediction2, average='weighted')
            #log_loss = metrics.log_loss(train_p['Class'].values, train_p_prediction2)
            #hamming_loss = metrics.hamming_loss(train_p['Class'].values, train_p_prediction2)
            
                       
            print ("Accuracy : %.4g" % accuracy_score)
            print ("AUC Score (Train): %f" % roc_auc_score)
            
            BO_res_file.write("%s " % accuracy_score)
            #BO_res_file.write("%s " % roc_auc_score)
            BO_res_file.write("%s " % average_precision_score)
            BO_res_file.write("%s " % precision_score)
            BO_res_file.write("%s " % recall_score)
            BO_res_file.write("%s " % f1_score)
            #BO_res_file.write("%s " % log_loss)
            #BO_res_file.write("%s " % hamming_loss)
            ######################################################                                
            ##############################################################################################################################
            ##############################################################################################################################
            
            
            test_prediction = gbm.predict(xgb.DMatrix(test[predictors], missing = 999), ntree_limit=gbm.best_iteration+1)
            test_prediction2 = test_prediction.round()
            
            test_p = test[test['Class'] == 1] 
            test_c = test[test['Class'] == 0] 
            test_p_prediction_orig = gbm.predict(xgb.DMatrix(test_p[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)
            test_p_prediction2 = test_p_prediction_orig.round()
            test_c_prediction_orig = gbm.predict(xgb.DMatrix(test_c[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)
            test_c_prediction2 = test_c_prediction_orig.round()
            
            
            ##############################################################################################################################
            ##############################################################################################################################
            ##############################################################################################################################
            ##############################################################################################################################
                           
            ##############################################################################################################################
            #OVERALL
            ##############################################################################################################################
            
            print ("############################################################################")
            print ("\nModel Report --- test set:")
           
            accuracy_score = metrics.accuracy_score(test['Class'].values, test_prediction2)
            roc_auc_score = metrics.roc_auc_score(test['Class'].values, test_prediction2)            
            average_precision_score = metrics.average_precision_score(test['Class'].values, test_prediction)           
            precision_score = metrics.precision_score(test['Class'].values, test_prediction2)
            recall_score = metrics.recall_score(test['Class'].values, test_prediction2)
            f1_score = metrics.f1_score(test['Class'].values, test_prediction2, average='weighted')
            #log_loss = metrics.log_loss(test['Class'].values, test_prediction2)
            #hamming_loss = metrics.hamming_loss(test['Class'].values, test_prediction2)
            
                       
            print ("Accuracy : %.4g" % accuracy_score)
            print ("AUC Score (Train): %f" % roc_auc_score)
            
            BO_res_file.write("%s " % accuracy_score)
            BO_res_file.write("%s " % roc_auc_score)
            BO_res_file.write("%s " % average_precision_score)
            BO_res_file.write("%s " % precision_score)
            BO_res_file.write("%s " % recall_score)
            BO_res_file.write("%s " % f1_score)
            #BO_res_file.write("%s " % log_loss)
            #BO_res_file.write("%s " % hamming_loss)
            
            ##############################################################################################################################
            #COSMICS
            ##############################################################################################################################
            
            accuracy_score = metrics.accuracy_score(test_c['Class'].values, test_c_prediction2)
            #roc_auc_score = metrics.roc_auc_score(test_c['Class'].values, test_c_prediction2)            
            average_precision_score = metrics.average_precision_score(test_c['Class'].values, test_c_prediction_orig)           
            precision_score = metrics.precision_score(test_c['Class'].values, test_c_prediction2)
            recall_score = metrics.recall_score(test_c['Class'].values, test_c_prediction2)
            f1_score = metrics.f1_score(test_c['Class'].values, test_c_prediction2, average='weighted')
            #log_loss = metrics.log_loss(test_c['Class'].values, test_c_prediction2)
            #hamming_loss = metrics.hamming_loss(test_c['Class'].values, test_c_prediction2)
                        
            print ("Accuracy : %.4g" % accuracy_score)
            print ("AUC Score (Test) cosmics: %f" % roc_auc_score)
            
            BO_res_file.write("%s " % accuracy_score)
            BO_res_file.write("%s " % roc_auc_score)
            BO_res_file.write("%s " % average_precision_score)
            BO_res_file.write("%s " % precision_score)
            BO_res_file.write("%s " % recall_score)
            BO_res_file.write("%s " % f1_score)
            #BO_res_file.write("%s " % log_loss)
            #BO_res_file.write("%s " % hamming_loss)
            ######################################################
                      
            ##############################################################################################################################
            #PBARS
            ##############################################################################################################################
            
            accuracy_score = metrics.accuracy_score(test_p['Class'].values, test_p_prediction2)
            #roc_auc_score = metrics.roc_auc_score(test_p['Class'].values, test_p_prediction2)            
            average_precision_score = metrics.average_precision_score(test_p['Class'].values, test_p_prediction_orig)           
            precision_score = metrics.precision_score(test_p['Class'].values, test_p_prediction2)
            recall_score = metrics.recall_score(test_p['Class'].values, test_p_prediction2)
            f1_score = metrics.f1_score(test_p['Class'].values, test_p_prediction2, average='weighted')
            #log_loss = metrics.log_loss(test_p['Class'].values, test_p_prediction2)
            #hamming_loss = metrics.hamming_loss(test_p['Class'].values, test_p_prediction2)
            
                       
            print ("Accuracy : %.4g" % accuracy_score)
            print ("AUC Score (Test) pbars: %f" % roc_auc_score)
            
            BO_res_file.write("%s " % accuracy_score)
            #BO_res_file.write("%s " % roc_auc_score)
            BO_res_file.write("%s " % average_precision_score)
            BO_res_file.write("%s " % precision_score)
            BO_res_file.write("%s " % recall_score)
            BO_res_file.write("%s " % f1_score)
            #BO_res_file.write("%s " % log_loss)
            #BO_res_file.write("%s " % hamming_loss)
            ######################################################                                
            ##############################################################################################################################
            ##############################################################################################################################
            
            
            
            
            
            
            
            
            
            """print (" ")
            print ("############################################################################")
            print ("Test set predictions....")
            
            #Predict test set:
            dtest_p = dtest[dtest['Class'] == 1] 
            dtest_c = dtest[dtest['Class'] == 0] 
            
            dtest_p_predictions_orig = gbm.predict(xgb.DMatrix(dtest_p[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)
            dtest_p_predictions = dtest_p_predictions_orig.round()
            dtest_c_predictions_orig = gbm.predict(xgb.DMatrix(dtest_c[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)
            dtest_c_predictions = dtest_c_predictions_orig.round()
            print ("Accuracy pbar: %.4g" % metrics.accuracy_score(dtest_p['Class'].values, dtest_p_predictions))
            print ("Accuracy cosmic: %.4g" % metrics.accuracy_score(dtest_c['Class'].values, dtest_c_predictions))
         
            
            test_prediction = gbm.predict(xgb.DMatrix(dtest[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)
            
            test_score_1 = average_precision_score(test[target].values, test_prediction)

            print('area under the precision-recall curve test set: {:.6f}'.format(test_score_1))
               
            test_prediction2 = test_prediction.round()
            test_score_2 = precision_score(test[target].values, test_prediction2)
            print('precision score: {:.6f}'.format(test_score_2))

            test_score_3 = recall_score(test[target].values, test_prediction2)
            print('recall score: {:.6f}'.format(test_score_3))
                   
            dtest_c_predictions = dtest_c_predictions_orig.round()
            
            print ("\nModel Report --- test set ")
            print ("Accuracy overall: %.4g" % metrics.accuracy_score(test[target].values, test_prediction2))
            #print "Accuracy pbar: %.4g" % metrics.accuracy_score(dtest_p['Class'].values, dtest_p_predictions)
            #print "Accuracy cosmic: %.4g" % metrics.accuracy_score(dtest_c['Class'].values, dtest_c_predictions)
            print ("AUC Score (Test): %f" % metrics.roc_auc_score(test['Class'], test_prediction2))
            #print " "

            
            print("\n Training finished. \n")
          
            # """
            
            ###################################################################################################################################################################
            ###################################################################################################################################################################
            
            """params = {
                    "objective": "binary:logistic",
                    "booster" : "gbtree",
                    "eval_metric": "auc",
                    #Log loss measures the compatability of estimated probabilities against the true outcomes. Since it is probabilistic, it does not suffer from class imbalance.
                    "eta": 0.0273,
                    #"tree_method": 'exact',
                    "max_depth": 14,
                    "subsample": 0.8827,
                    "colsample_bytree": 0.9840,
                    "max_delta_step": 0.9709,
                    "silent": 1,
                    "min_child_weight": 0.4133,
                    "seed": 27,
                    #"alpha" : 0.6,
                    "gamma" : 0.8428
                    #"num_class" : 22,
            }  

            early_stopping_rounds = 20 
            watchlist = [(dtrain, 'train'), (dtest, 'eval')]
            #gbm = xgb.train(params, dtrain, 800, evals=watchlist,  early_stopping_rounds=early_stopping_rounds, verbose_eval=True)
            gbm_old = xgb.train(params, dtrain, 300, evals=watchlist, verbose_eval=False) 

            ####################################################################################


            print (" ")
            print ("############################################################################")
            
            print("on TRaining set...")
            
            gbm = gbm_old
            
            train_prediction = gbm.predict(xgb.DMatrix(train[predictors], missing = 999), ntree_limit=gbm.best_iteration+1)
            train_prediction2 = train_prediction.round()
                
            ##############################################################################################################################
            ##############################################################################################################################
            
            print ("############################################################################")
            print ("\nModel Report --- train set:")
            print ("Accuracy : %.4g" % metrics.accuracy_score(train['Class'].values, train_prediction2))
            print ("Accuracy : %.4g" % metrics.accuracy_score(train['Class'].values, train_prediction))
            print ("AUC Score (Train): %f" % metrics.roc_auc_score(train['Class'], train_prediction2))
            print ("AUC Score (Train): %f" % metrics.roc_auc_score(train['Class'], train_prediction))
            print ("AUC Score (Train): %f" % metrics.roc_auc_score(train['Class'].values, train_prediction2))
            ###################################################### 
               
                
            ##############################################################################################################################
            ##############################################################################################################################
            
            print (" ")
            print ("############################################################################")
            print ("Test set predictions....")
            
            #Predict test set:
            dtest_p = test[test['Class'] == 1] 
            dtest_c = test[test['Class'] == 0] 
            
            dtest_p_predictions_orig = gbm.predict(xgb.DMatrix(dtest_p[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)
            dtest_p_predictions = dtest_p_predictions_orig.round()
            dtest_c_predictions_orig = gbm.predict(xgb.DMatrix(dtest_c[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)
            dtest_c_predictions = dtest_c_predictions_orig.round()
            print ("Accuracy pbar: %.4g" % metrics.accuracy_score(dtest_p['Class'].values, dtest_p_predictions))
            print ("Accuracy cosmic: %.4g" % metrics.accuracy_score(dtest_c['Class'].values, dtest_c_predictions))
         
            
            test_prediction = gbm.predict(xgb.DMatrix(test[predictors],missing = 999), ntree_limit=gbm.best_iteration+1)
            
            test_score_1 = average_precision_score(test[target].values, test_prediction)

            print('area under the precision-recall curve test set: {:.6f}'.format(test_score_1))
               
            test_prediction2 = test_prediction.round()
            test_score_2 = precision_score(test[target].values, test_prediction2)
            print('precision score: {:.6f}'.format(test_score_2))

            test_score_3 = recall_score(test[target].values, test_prediction2)
            print('recall score: {:.6f}'.format(test_score_3))
                   
            dtest_c_predictions = dtest_c_predictions_orig.round()
            
            print ("\nModel Report --- test set ")
            print ("Accuracy overall: %.4g" % metrics.accuracy_score(test[target].values, test_prediction2))
            print ("Accuracy pbar: %.4g" % metrics.accuracy_score(test['Class'].values, test_predictions))
            print ("Accuracy cosmic: %.4g" % metrics.accuracy_score(test['Class'].values, test_predictions))
            print ("AUC Score (Test): %f" % metrics.roc_auc_score(test['Class'], test_prediction2))
            #print " "

            # """
            
           

            print ("----------------------------------------------------------------------------------------------------------------------------------------------------------------")


  
