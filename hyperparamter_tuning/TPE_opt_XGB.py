from __future__ import print_function
#Import libraries:
import sys
import pandas as pd
import numpy as np

import matplotlib.pylab as plt

from matplotlib.pylab import rcParams

import hyperopt

from hyperopt import hp, tpe
from hyperopt.fmin import fmin

from sklearn.model_selection import cross_val_score, StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import make_scorer

import xgboost as xgb

#import lightgbm as lgbm

from sklearn.model_selection import KFold

from MLsrc.prepare_data_final import *

#from imblearn.ensemble import BalancedBaggingClassifier

#from sklearn.metrics import mean_squared_error
from operator import itemgetter
import random
import time
#from sklearn.grid_search import GridSearchCV
#from sklearn.metrics import average_precision_score
#from numpy import genfromtxt
#import seaborn as sns
#from sklearn import preprocessing
#from sklearn.metrics import roc_curve, auc,recall_score,precision_score
import datetime as dt
from sklearn.metrics import roc_auc_score

from functools import partial

#from sklearn.dummy import DummyClassifier


#from subprocess import check_output
#print ((check_output(["ls", "../input"]).decode("utf8"))
#from xgboost import plot_tree

def grid_scores_to_df(grid_scores):
    """
    Convert a sklearn.grid_search.GridSearchCV.grid_scores_ attribute to a tidy
    pandas DataFrame where each row is a hyperparameter-fold combinatination.
    """
    rows = list()
    for grid_score in grid_scores:
        for fold, score in enumerate(grid_score.cv_validation_scores):
            row = grid_score.parameters.copy()
            row['fold'] = fold
            row['score'] = score
            rows.append(row)
    df = pd.DataFrame(rows)
    return df


######################################################################################################  
######################################################################################################  

def create_feature_map(features):
    outfile = open('../xgb.fmap', 'w')
    for i, feat in enumerate(features):
        outfile.write('{0}\t{1}\tq\n'.format(i, feat))
    outfile.close()

######################################################################################################  
######################################################################################################  
def get_importance(gbm, features):
    create_feature_map(features)
    importance = gbm.get_fscore(fmap='xgb.fmap')
    importance = sorted(importance.items(), key=itemgetter(1), reverse=True)
    return importance
######################################################################################################  
######################################################################################################  
def get_features(train, test):
    trainval = list(train.columns.values)
    output = trainval
    return sorted(output)

######################################################################################################  
######################################################################################################

def gini(truth, predictions):
    g = np.asarray(np.c_[truth, predictions, np.arange(len(truth)) ], dtype=np.float)
    g = g[np.lexsort((g[:,2], -1*g[:,1]))]
    gs = g[:,0].cumsum().sum() / g[:,0].sum()
    gs -= (len(truth) + 1) / 2.
    return gs / len(truth)

def gini_xgb(predictions, truth):
    truth = truth.get_label()
    return 'gini', -1.0 * gini(truth, predictions) / gini(truth, truth)

def gini_lgb(truth, predictions):
    score = gini(truth, predictions) / gini(truth, truth)
    return 'gini', score, True

def gini_sklearn(truth, predictions):
    return gini(truth, predictions) / gini(truth, truth)

######################################################################################################  
######################################################################################################



######################################################################################################  
######################################################################################################

def objective(params):
    #print ("ha!3")

    params = {
        'n_estimators' : int(params['n_estimators']),
        'learning_rate' : "{:.3f}".format(params['learning_rate']),
        'max_depth': int(params['max_depth']),
        'gamma': "{:.3f}".format(params['gamma']),        
        'colsample_bytree': "{:.3f}".format(params['colsample_bytree']),
        'min_child_weight' : int(params['min_child_weight']),
        'subsample': "{:.3f}".format(params['subsample']),
        'max_delta_step': '{:.3f}'.format(params['max_delta_step']),
        'reg_lambda': "{:.3f}".format(params['reg_lambda']), 
        'reg_alpha': "{:.3f}".format(params['reg_alpha'])
            }
    #print( "ha!4")
    clf = xgb.XGBClassifier(
        #n_estimators=500,
        #learning_rate=0.1,
        **params
    )
    
    #balanced_random_forest = BalancedBaggingClassifier(base_estimator=clf, random_state=0)

    """balanced_random_forest.fit(X_train, y_train)
    print('Classification results using a balanced random forest classifier on'
          ' imbalanced data')
    y_pred_balanced_rf = balanced_random_forest.predict(X_test)
    print(classification_report_imbalanced(y_test, y_pred_balanced_rf))
    cm_bagging = confusion_matrix(y_test, y_pred_balanced_rf)
    plt.figure()
    plot_confusion_matrix(cm_bagging, classes=np.unique(ozone.target),
                          title='Confusion matrix using balanced random forest')

    plt.show()"""




    
    #print ("ha!5")
    score = cross_val_score(clf, X, Y, scoring=gini_scorer, cv=StratifiedKFold()).mean()
    print (("Gini {:.3f} params {}".format(score, params)))
    return score
######################################################################################################  
######################################################################################################

from datetime import datetime 
if __name__ == '__main__':

    #if len(argv) < 2:
    #    print ( "usage: ", argv[0], "algorithm, sampling"
    #    exit(-1)
        
    rcParams['figure.figsize'] = 12, 4

    ######################################################################################################
    # Target variable: variable that is or should be the output.(binary 0 or 1 if you are classifying)
    #
    # Predictor variables: input data or the variables that is mapped to the target variable 
    #
    # score: Returns the mean accuracy on the given test data and labels
    #
    # effect of weighting the addition of new trees to a gradient boosting model, called shrinkage or the learning rate
    # That adding a learning rate is intended to slow down the adaptation of the model to the training data.
    # The learning rate parameter ([0,1]) in Gradient Boosting shrinks the contribution of
    # each new base model - typically a shallow tree - that is added in the series.
    #
    #
    # The idea of boosted decision trees is to first train a classifier using the training data
    # and to use, for the next training iteration, a modified training sample in which the
    # previously misclassified events are given a larger weight. This procedure is then iterated,
    # and finally the result of all the different classifiers obtained is averaged. The final
    # classifier is then a linear combination of the so-called base classifiers,
    #
    #
    # Different boosting algorithms, that is different prescriptions of how the event
    # weights are updated in each training step and how the various base classifiers are
    # weighted in the final linear combination, correspond to different loss functions
    # used in the stepwise minimisation 

    
    ######################################################################################################
    np.set_printoptions(threshold=None)
    pbardata = np.loadtxt("training_data/all_pbars_data_2016_yangle.dat")
    cosmicdata = np.loadtxt("training_data/all_cosmics_data_2016_halfbreak1_yangle.dat")
    #tot_c = 314776.0 # half break 2... BGO trigger
    #tot_c_time = 197074.0 # half second break
    tot_c = 282935.0 # half break 1... hodoscope+BGO trigger
    tot_c_time = 603659.0 # in seconds, half break 1... hodoscope+BGO trigger
    cosmic_rate = tot_c/tot_c_time
    
    #cosmicdata = cosmicdata[cosmicdata[:,3] > 3]
    #pbardata = pbardata[pbardata[:,3] > 3]
    
    print ( "-----------------------------------------------------------------------------------------------")
    print ( "----------------------------------- START OF ANALYSIS -----------------------------------------")
    print ( "-----------------------------------------------------------------------------------------------")
    print(( "tot cosmics: all events recorded, events in cosmic data file", tot_c, len(cosmicdata)))
    print(( "cosmic rate", cosmic_rate))
    #tot_p = 25934.0
    #hbardata_orig = np.loadtxt('./all_events_cusp_eventnr_mergemore.dat') # all events in the low positron density mixing runs
    #hbardata_orig = np.loadtxt('./pbars_2017_tracking_data.dat')
    
    
    # output file for cosmic rejection, pbar efficiency etc
    #xg_vals = open('./xgboost_parameter_gridsearch/xgb_scan_learning_rate_vs_trees_smote_1.dat', 'w')
          
    #samp = "SMOTE"     # what sampling method to overcome imbalanced data?
    samplings = ["none"]#, "SMOTETomek", "SMOTEENN"] #["SMOTE", "SMOTETomek", "SMOTEENN"]
    test_size = 0.20   # size of test sample
    seed = seed = random.seed(datetime.now())          # random seed
                
    bool_cut_vertex = True      # cut on vertex ?
    track_cut = 0               # events with tracks > track_cut will be accepted
    BGO_cut = 0.0
    validbool = False           # validation sample?
    valid_size = 0              # size of validation sample?
    stratified_testing = False  # stratified test sample means: when splitting the total sample in train and test,
                                # keep the class ratios (of whole sample) in the test sample. (if there are 10x more cosmics,
                                # in the test sample there will also be 10x more cosmics) 
    
    target = 'Class'   
        
      
    #####################################################################################################
    #####################################################################################################
    # xgboost style... for sklearn style with wrapper see below
        
    nr_of_xgboosts = 5
    
    for samp in samplings:
        
        #print ( txtfile    
        for i in range(nr_of_xgboosts): 
            #xg_vals = open('./xgboost_parameter_gridsearch/hodortrigger/xgb_hyperopt_tpe_run{0}_{1}.dat'.format(i, samp), 'w')
          
               
            print ( "-----------------------------------------------------------------------------------------------")
            print ( "----------------------------------------PREPARE DATA ------------------------------------------")
            train2, test, estimated_cosmics, cosmic_test_ratio, tot_p, tot_p_aftertrack = prepare_data(tot_c_time, pbardata, cosmicdata, samp, test_size, seed, bool_cut_vertex, track_cut, BGO_cut, validbool, valid_size, stratified_testing)
            print ( "-----------------------------------------------------------------------------------------------")
            #hbardata = prepare_hbardata(hbardata_orig)
                       
            predictors = [x for x in train2.columns if x not in [target]] 


            print(( "predictors: ", predictors))
    
            frames = [train2, test]

            train = pd.concat(frames)
            
            X = train[predictors]

            Y = train[target]#.reshape(len(train[target]),1)           
           

            """print "random choice ........."
            X_cosmics = ((train[train[target]==0])[predictors]).as_matrix()
            
            X_pbars = ((train[train[target]==1])[predictors]).as_matrix()

            pick_from_ = np.arange(0, len(X_cosmics))
            #print "pick ", pick_from_.shape
            inds_ = np.random.choice(pick_from_, size = int(len(X_pbars)))
            #
            #print inds
            #print "len inds", inds_.shape
            #print "cos shape", X_cosmics.shape   
            
            #inds_ = inds_.reshape((len(inds_),1))         
            X_cosmics = X_cosmics[inds_]
            
          
            
            y_cosmics = np.zeros(len(X_cosmics))
            y_pbars = np.ones(len(X_pbars))
            print X_cosmics.shape
            
            print " "  

            #X_ = np.vstack((X_pbars, X_cosmics))
            #y_ = np.hstack((y_pbars, y_cosmics))
            
            #print X_.shape
            print X_cosmics.shape
            print X_pbars.shape
            
            # shuffle
            dataset = np.vstack((np.hstack((X_pbars, y_pbars.reshape(len(y_pbars),1))), np.hstack((X_cosmics, y_cosmics.reshape(len(y_cosmics),1)))))
            dataset = np.take(dataset,np.random.permutation(dataset.shape[0]),axis=0,out=dataset)
            
            X = dataset[:,[0,1,2,3,4,5,6,7,8,9]]
            Y= dataset[:,10]"""
            
            
            print(X.shape, Y.shape)
            
            
            #exit(0)
            
            
            
            
            #print "predictors: ", predictors

            
            #X_test = test[predictors]
            #X_test, scaler = scale_data(X_test1)
            #y_test = test[target].reshape(len(test[target]),1)
           
                        
            start_time = dt.datetime.now()
            print (("Start time: ",start_time))

            print (("Building model.. ",dt.datetime.now()-start_time))
        
            print ( "-----------------------------------------------------------------------------------------------")
            print ( "----------------------------------------- XGB RUN ---------------------------------------------")
            print(( "--------------------------------------", samp, "-----------------------------------------------"))
            
        
            gini_scorer = make_scorer(gini_sklearn, greater_is_better=True, needs_proba=True)
            print ("ha!1")
            space = {
                'n_estimators': hp.quniform('n_estimators', 200, 5000, 1),
                'learning_rate': hp.uniform('learning_rate', 0.0, 1.0),
                'max_depth': hp.quniform('max_depth', 2, 30, 1),
                'colsample_bytree': hp.uniform('colsample_bytree', 0.3, 1.0),
                'gamma': hp.uniform('gamma', 0.0, 1.0),
                'min_child_weight': hp.quniform('min_child_weight', 0, 30, 1),
                'subsample': hp.uniform('subsample',0.01, 1.0),
                'max_delta_step': hp.uniform('max_delta_step',0.01, 5),
                'reg_lambda': hp.uniform('reg_lambda', 0.0, 1.0),
                'reg_alpha': hp.uniform('reg_alpha', 0.0, 1.0)
            }
            
            #params = {'max_depth':3, 'colsample_bytree':0.4, 'gamma': 0.1}            
            #sore = objective(params)

            #print ("ha!2")

            trials = hyperopt.Trials()

            tpe = partial(
                hyperopt.tpe.suggest,

                # Sample 1000 candidate and select candidate that
                # has highest Expected Improvement (EI)
                n_EI_candidates=400,

                # Use 20% of best observations to estimate next
                # set of parameters
                gamma=0.2,

                # First XX trials are going to be random
                n_startup_jobs=200
            )

            best = fmin(fn=objective, space=space, algo=tpe, trials=trials, max_evals=1)
            

            print ( "Hyperopt estimated optimum {}".format(best))

            

