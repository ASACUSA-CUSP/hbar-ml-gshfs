from __future__ import print_function
#Import libraries:
import pandas as pd
import numpy as np
import xgboost as xgb
from xgboost.sklearn import XGBClassifier   #xgboost module in python has an sklearn wrapper called XGBClassifier. It uses sklearn style naming convention.
from sklearn import  metrics   #Additional scklearn functions
#from sklearn.grid_search import GridSearchCV   #Perforing grid search

import matplotlib.pylab as plt
#%matplotlib inline
from matplotlib.pylab import rcParams
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold

import ROOT
from root_numpy.tmva import add_classification_events, evaluate_reader
from root_numpy import ROOT_VERSION
from ROOT import TMVA, TFile, TCut

from array import array

#from MLsrc.sampling import *
#from MLsrc.train_test_split import *

import sys

sys.path.append("..")


from MLsrc.prepare_data_testing_feature_select_use_this_one2_new2_plot import *

from sklearn.model_selection import train_test_split 

#import scipy.stats as stats

#from xgboost import plot_tree

######################################################################################################  
######################################################################################################  
if __name__ == '__main__':

    #if len(argv) < 2:
    #    print "usage: ", argv[0], "algorithm, sampling"
    #    exit(-1)

    
        
    rcParams['figure.figsize'] = 12, 4
    
    ######################################################################################################
    # Target variable: variable that is or should be the output.(binary 0 or 1 if you are classifying)
    #
    # Predictor variables: input data or the variables that is mapped to the target variable 
    #
    # score: Returns the mean accuracy on the given test data and labels
    #
    # effect of weighting the addition of new trees to a gradient boosting model, called shrinkage or the learning rate
    # That adding a learning rate is intended to slow down the adaptation of the model to the training data.
    # The learning rate parameter ([0,1]) in Gradient Boosting shrinks the contribution of
    # each new base model - typically a shallow tree - that is added in the series.
    #
    #
    # The idea of boosted decision trees is to first train a classifier using the training data
    # and to use, for the next training iteration, a modified training sample in which the
    # previously misclassified events are given a larger weight. This procedure is then iterated,
    # and finally the result of all the different classifiers obtained is averaged. The final
    # classifier is then a linear combination of the so-called base classifiers,
    #
    #
    # Different boosting algorithms, that is different prescriptions of how the event
    # weights are updated in each training step and how the various base classifiers are
    # weighted in the final linear combination, correspond to different loss functions
    # used in the stepwise minimisation 

    
    ######################################################################################################
    np.set_printoptions(threshold=np.nan)
    pbardata = np.loadtxt("../training_data/all_pbars_data_2016_yangle.dat")
    cosmicdata = np.loadtxt("../training_data/all_cosmics_data_2016_halfbreak1_yangle.dat")
    #tot_c = 314776.0 # half break 2... BGO trigger
    #tot_c_time = 197074.0 # half second break
    tot_c = 282935.0 # half break 1... hodoscope+BGO trigger
    tot_c_time = 603659.0 # in seconds, half break 1... hodoscope+BGO trigger
    cosmic_rate = tot_c/tot_c_time
    #hbardata_orig = np.loadtxt('./all_events_cusp_eventnr_mergemore.dat') # all events in the low positron density mixing runs
    #algorithm = argv[1]
    
    samp = "custom"
    test_size = 0.5
    seed = 7    
    validbool = False
    valid_size = 0.0
    stratified_testing = True
    
    bool_cut_vertex = True # cut on vertex ?
    track_cut = 0
    BGO_cut = 0.0
  
    
    #xg_vals = open('./hbars_xgboost3/test_vals_1tr.dat', 'w')
    xg_vals = open ('test.dat', 'w')
    
    
    #########################################################################################################################    
    target = 'Class'
           
    #samp = "SMOTE"     # what sampling method to overcome imbalanced data?

    #scan_mode = True
    train, test, estimated_cosmics, cosmic_test_ratio, tot_p, tot_p_aftertrack = prepare_data(tot_c_time, pbardata, cosmicdata, samp, test_size, seed, bool_cut_vertex, track_cut, BGO_cut, validbool, valid_size, stratified_testing)
        
    print("len test cosmic/pbar", len(test[test[target]==0]), len(test[test[target]==1]))
    #print "len(valid) cosmic/pbar", len(valid[valid[target]==0]),len(valid[valid[target]==1])
    print("len train cosmic/pbar", len(train[train[target]==0]), len(train[train[target]==1]))
     
    print("tot_p", tot_p)
    print("tot_p after track: ", tot_p_aftertrack)


    #print "tot_p test",len(test['Class' == 1])
    #print "tot_c test",len(test['Class' == 0])

    tot_p_test = tot_p*test_size    

    #exit(0)
    print(train.iloc[0])  # print first row of train data
 
    #predictors = [x for x in train.columns if x not in [target]] 
    
    predictors  = ['BGO_E', 'inner_hits', 'outer_hits']
    #print predictors  
    
    #exit(0)
    
    n_vars = len(predictors)
    
    X_train = train[predictors]
    
    """# mark zero values as missing or NaN
    X_orig = X_train.replace(999, np.NaN)
    
    print(np.isnan(X_orig).sum())

    # fill missing values with mean column values
    imputer = Imputer()
    X_train = imputer.fit_transform(X_orig)
    
    print(np.isnan(X).sum())"""
    
    
    
    
    
    y_train = train[target].reshape(len(train[target]),1)   
    X_train = X_train.as_matrix()
    
    #print X_train

    X_test = test[predictors]
    y_test = test[target].reshape(len(test[target]),1)
    X_test = X_test.as_matrix()
    
       
    #n_vars = 1
    #########################################################################################################################
    
    # TMVA rect cuts compare:
    
    
    
    
    
    output = TFile('tmva_output_trackcut_{}.root'.format(track_cut), 'recreate')
    
    factory = TMVA.Factory('classifier', output,
                       ":".join([
                                "!V",
                                "!Silent",
                                "Color",
                                "DrawProgressBar",
                                #"Transformations=I;D;P;G,D",
                                "AnalysisType=Classification"]
                                ))
             
    if ROOT_VERSION >= '6.07/04':
            data = TMVA.DataLoader('blabla')
    else:
        data = factory
        
    for n in range(n_vars):
        name = predictors[n]
        data.AddVariable(name, 'F')
        print(name)
        
    # Call root_numpy's utility functions to add events from the arrays
    add_classification_events(data, X_train, y_train) #, weights=w_train)
    
    add_classification_events(data, X_test, y_test, test=True)  #, weights=w_train)
    
    
    # The following line is necessary if events have been added individually:
    data.PrepareTrainingAndTestTree(TCut('1'), 'NormMode=EqualNumEvents')

    if ROOT_VERSION >= '6.07/04':
        BookMethod = factory.BookMethod
    else:
        BookMethod = TMVA.Factory.BookMethod
        
     
    # add methods that you want to test on the data
    
    BookMethod(data, 'Cuts', 'testCuts') # simple rectangular cuts
    
    #BookMethod(data, ROOT.TMVA.Types.kKNN, 'KNN',"H:nkNN=20:ScaleFrac=0.8:SigmaFact=1.0:Kernel=Gaus:UseKernel=F:UseWeight=T:!Trim" ) 
    
    """BookMethod(data, ROOT.TMVA.Types.kBDT, 'BDT',
                   ":".join([
                       "!H",
                       "!V",
                       "NTrees=350",
                       "nEventsMin=150",
                       "MaxDepth=3",
                       "BoostType=AdaBoost",
                       "AdaBoostBeta=0.5",
                       "SeparationType=GiniIndex",
                       "nCuts=20",
                       "PruneMethod=NoPruning",
                       ]))     """
                       
                       
    # automatic optimization of parameters                   
    factory.OptimizeAllMethods("ROCIntegral","Minuit")   
    
    #exit(0)   
    
    
    
    # TODO
    
    # try this
    #Modify the binning in the ROC curve (for classification only)
    TMVA.gConfig().GetVariablePlotting().fNbinsXOfROCCurve = 300
                       
                       
                         
                          
    print("TRAIN ..............................................................")           
    factory.TrainAllMethods()


    print("Test All Methods ..............................................................")
    factory.TestAllMethods()
    
    
    print("Evaluate All Methods ..............................................................")
    factory.EvaluateAllMethods()

    #roc = factory.GetROCCurve(data)
    #print "roc object"
    #print roc
           

    #exit(0)

    print("TEST ..............................................................")
    # Classify the test dataset with the classifier
    reader = TMVA.Reader()        
           
    for n in range(n_vars):
        name = predictors[n]
        reader.AddVariable(name, array('f', [0.]))
        print(name)
    
    #exit(0)    
        
        
    reader.BookMVA('Cuts', 'weights/classifier_testCuts.weights.xml')
    twoclass_output = evaluate_reader(reader, 'Cuts', X_test)

    print(twoclass_output.shape)
    twoclass_output = twoclass_output.reshape(len(twoclass_output),1)

    plot_colors = "br"
    plot_step = 0.02
    class_names = "AB"
    cmap = plt.get_cmap('bwr')

    fig = plt.figure(figsize=(10, 5))
    fig.patch.set_alpha(0)

    plt.title('Decision Boundary')

    # Plot the two-class decision scores
    ax = plt.subplot(122)
    ax.xaxis.grid(False)
    for i, n, c in zip([0, 1], class_names, plot_colors):
        plt.hist(twoclass_output[y_test == i],
                 bins=20,
                 range=(-4, 4),
                 facecolor=c,
                 label='Class %s' % n,
                 alpha=.5, histtype='stepfilled')
                 
    x1, x2, y1, y2 = plt.axis()
    plt.axis((x1, x2, y1, 140))
    plt.legend(loc='upper right')
    plt.ylabel('Samples')
    plt.xlabel('Score')
    plt.title('Decision Scores')

    plt.tight_layout()
    plt.subplots_adjust(wspace=0.25)
    plt.show()      
                                     
                                     
                  
    
    """s
    
    hbardata = prepare_hbardata(hbardata_orig)
   
    target = 'Class'  
    
    #exit(0) 
    
    predictors = [x for x in train.columns if x not in [target]] 

    print "predictors: ", predictors"""
    
