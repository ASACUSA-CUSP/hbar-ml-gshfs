from __future__ import print_function
#Import libraries:
import pandas as pd
import numpy as np
import xgboost as xgb
from xgboost.sklearn import XGBClassifier   #xgboost module in python has an sklearn wrapper called XGBClassifier. It uses sklearn style naming convention.
#from sklearn import cross_validation, metrics   #Additional scklearn functions
#from sklearn.grid_search import GridSearchCV   #Perforing grid search

import matplotlib.pylab as plt
#%matplotlib inline
from matplotlib.pylab import rcParams
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import GridSearchCV

import sys
sys.path.append("..")

#from MLsrc.sampling import *
#from MLsrc.train_test_split import *
from MLsrc.prepare_data_final import *
"""
from sklearn.svm import LinearSVC
from sklearn.feature_selection import SelectFromModel
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2, f_classif
from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from sklearn.ensemble import ExtraTreesClassifier
"""

from mlxtend.feature_selection import SequentialFeatureSelector as SFS
#from sklearn.neighbors import KNeighborsClassifier
#from mlxtend.data import wine_data
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline

#from mlxtend.plotting import plot_sequential_feature_selection as plot_sfs

from sklearn.model_selection import PredefinedSplit

#from sklearn.feature_selection import RFECV


######################################################################################################  
######################################################################################################  
if __name__ == '__main__':

    #if len(argv) < 2:
    #    print "usage: ", argv[0], "algorithm, sampling"
    #    exit(-1)
        
    rcParams['figure.figsize'] = 12, 4

    #changed from np.nan to 1000 (default) GIOVANNI
    np.set_printoptions(threshold=1000)
    pbardata = np.loadtxt("../training_data/all_pbars_data_2016_yangle.dat")
    cosmicdata = np.loadtxt("../training_data/all_cosmics_data_2016_halfbreak1_yangle.dat")
    #tot_c = 314776.0 # half break 2... BGO trigger
    #tot_c_time = 197074.0 # half second break
    tot_c = 282935.0 # half break 1... hodoscope+BGO trigger
    tot_c_time = 603659.0 # in seconds, half break 1... hodoscope+BGO trigger
    cosmic_rate = tot_c/tot_c_time
    
    #cosmicdata = cosmicdata[cosmicdata[:,3] > 3]
    #pbardata = pbardata[pbardata[:,3] > 3]
    
    samp = "custom"     # what sampling method to overcome imbalanced data?
    test_size = 0.33   # size of test sample
    seed = 7           # random seed
    
    BGO_cut = 0          
    bool_cut_vertex = True      # cut on vertex ?
    track_cut = 0               # events with tracks > track_cut will be accepted
    validbool = False           # validation sample?
    valid_size = 0              # size of validation sample?
    stratified_testing = True  # stratified test sample means: when splitting the total sample in train and test,
                                # keep the class ratios (of whole sample) in the test sample. (if there are 10x more cosmics,
                                # in the test sample there will also be 10x more cosmics) 
                                
                                
          
            
    print("-----------------------------------------------------------------------------------------------")
    print("----------------------------------------PREPARE DATA ------------------------------------------")
    train, test, estimated_cosmics, cosmic_test_ratio, tot_p, tot_p_aftertrack = prepare_data(tot_c_time, pbardata, cosmicdata, samp, test_size, seed, bool_cut_vertex, track_cut, BGO_cut, validbool, valid_size, stratified_testing)
    print("-----------------------------------------------------------------------------------------------")
        
    #hbardata = prepare_hbardata(hbardata_orig)
   
    target = 'Class'  
    
    #exit(0) 
    
    predictors = [x for x in train.columns if x not in [target]] 

    print("predictors: ", predictors)
    
    X = train[predictors]
    Y = train[target]
    Y = Y.values.reshape(len(Y),1)
    
    X_test = test[predictors]
    Y_test = test[target]
    
    #print len(Y_test[Y_test == 1]),  len(Y_test[Y_test == 0])
    #print len(Y[Y == 1]),  len(Y[Y == 0])
    
    #exit(0)
    
    Y_test = Y_test.values.reshape(len(Y_test), 1)
    
    
    X_new = np.vstack([X_test, X])
    
    Y_new = np.vstack([Y_test, Y])
    
    
    
    #Y_new = np.append(Y_test, Y)
    
    print(Y_new.shape)
    
    print(Y_new[0].shape)
    
    Y_new = Y_new.reshape(len(Y_new),).astype(int)
    
    
    #exit(0)
    my_test_fold = []

    # put -1 here, so they will be in training set
    # for all greater indices, assign 0, so they will be put in test set
    for i in range(len(X_test)):
        my_test_fold.append(0)

    
    for i in range(len(X)):
        my_test_fold.append(-1)
    
   
    piter = PredefinedSplit(my_test_fold)
    
    print(X.shape)
    print(X_test.shape)
    print(X_new.shape)
    
    
    print(Y_new[np.arange(len(Y_test))])
    
    #exit(0)
    
    #print X.shape
    
    
    

    ######################################################################################################   
    print("------------------------ FEATURE SELCTION START --------------------------")


    model = XGBClassifier(
     learning_rate = 0.09204513386599818, #best_lr,                   
     n_estimators = 3700, #best_nest,    # The number of sequential trees to be modeled
     max_depth=20,
     min_child_weight=0.0,
     gamma=0.04755589354076043,              # A node is split only when the resulting split gives a positive reduction in the loss function. Gamma specifies the minimum loss reduction required to make a split. Makes the algorithm conservative. The values can vary depending on the loss function and should be tuned.
     subsample=0.9224434653576445,        # Denotes the fraction of observations to be randomly samples for each tree.
     colsample_bytree=0.7964006923073769, # Similar to max_features in GBM. Denotes the fraction of columns to be randomly samples for each tree.
     objective= 'binary:logistic',
     eval_metric = "auc",
     n_jobs=10,
     missing = 999,
     max_delta_step=4.996698512573651,
     reg_alpha = 0.21876916567664978,
     reg_lambda = 0.09879847889762797,
     early_stopping_rounds = 20,
     #reg_alpha = 0.6, 
     #scale_pos_weight=1,
     random_state=27)  

    sfs1 = SFS(estimator=model, 
           k_features=1,
           forward=True,
           floating=False,
           scoring='roc_auc',
           cv=piter)

    pipe = make_pipeline(StandardScaler(), sfs1)

    resulting_x = pipe.fit(X_new, Y_new)

    

    print('best combination (ACC: %.3f): %s\n' % (sfs1.k_score_, sfs1.k_feature_idx_))
    print(('all subsets:\n', sfs1.subsets_))

    #np.savetxt('resulting_x_test.dat',  resulting_x, delimiter=',')
    

   # plot_sfs(sfs1.get_metric_dict(), kind='std_err')

    


    """sbs1 = SFS(estimator=model, 
           k_features=(1, 10),
           forward=False,
           #backward = True,  
           floating=False, 
           scoring='accuracy',
           cv=7)

    pipe = make_pipeline(StandardScaler(), sbs1)

    pipe.fit(X, Y)

    print('best combination (ACC: %.3f): %s\n' % (sbs1.k_score_, sbs1.k_feature_idx_))
    print('all subsets:\n', sbs1.subsets_)
    plot_sfs(sbs1.get_metric_dict(), kind='std_err')"""

    plt.savefig("sfs_plot.pdf")

    #plt.show()


