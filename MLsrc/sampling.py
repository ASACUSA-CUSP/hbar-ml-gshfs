from __future__ import print_function
import matplotlib.pyplot as plt
import numpy as np

import sklearn

import imblearn

from imblearn.over_sampling import RandomOverSampler, SMOTE
#from imblearn.combine import SMOTEENN, SMOTETomek

#from imblearn.under_sampling import TomekLinks
#from imblearn.under_sampling import RandomUnderSampler
from imblearn.under_sampling import ClusterCentroids




def combinedsample_custom_data(X, y, whichone_over, whichone_under, RANDOM):
    print("----------------------------------------------------------------------------") 
    print("Custom combination of Over- and Undersampling test...")
    sampler = None
    
    X_cosmics = X[y == 0]
    X_pbars   = X[y == 1]
    y_pbars = np.ones(len(X_pbars))
      
    print(X.shape)
    print(X_cosmics.shape)
    print(X_pbars.shape)
    
    print(" ")

    if whichone_under == "Random":
        print("random choice .........")
        pick_from = np.arange(0, len(X_cosmics))
        #print pick_from
        inds = np.random.choice(pick_from, size = int(len(X_cosmics)*0.30))
        
        #print "samp!", inds.shape
        
        X_cosmics_under = X_cosmics[inds]
        
        
        
        #print "samp!", X_cosmics.shape
        y_cosmics = np.zeros(len(X_cosmics_under))
        print(X_cosmics_under.shape)
        
        print(" ")

    if whichone_over == "SMOTE": 
        print("SMOTE.........")       
        sampler = SMOTE(random_state=RANDOM) #remove deprecated argument ind


    X = np.vstack((X_pbars, X_cosmics_under))
    y = np.hstack((y_pbars, y_cosmics))
    
    # TODO shuffle?
    
    print(X.shape)
        
    X_resampled, y_resampled = sampler.fit_resample(X, y)

    print(X_resampled.shape, y_resampled.shape)

    pick_from2 = np.arange(0, len(X_resampled))
    #print pick_from2
    np.random.shuffle(pick_from2)

    #print shuffled_inds
    #print shuffled_inds.shape

    X_resampled = X_resampled[pick_from2]
    y_resampled = y_resampled[pick_from2]


    print(X_resampled.shape, y_resampled.shape)

    print("Number of pbar events before oversampling: ", len(X[y == 1]))
    print("Number of cosmic events before oversampling: ", len(X[y == 0]))
    print(" ")
    print("Number of pbar events after oversampling: ", len(X_resampled[y_resampled == 1]))
    print("Number of cosmic events after oversampling: ", len(X_resampled[y_resampled == 0]))
    
    return X_resampled, y_resampled

#########################################################################################################  
#########################################################################################################  

def combinedsample_data(X, y, whichone, RANDOM):
    print("----------------------------------------------------------------------------") 
    print("Combination of Over- and Undersampling test...")
    sampler = None

    if whichone == "SMOTETomek":
        print("SMOTE + Tomek.........")
        sampler = SMOTETomek(random_state = RANDOM)

    if whichone == "SMOTEENN": 
        print("SMOTE + ENN.........")       
        sampler = SMOTEENN(random_state = RANDOM)



    X_resampled, y_resampled = sampler.fit_sample(X, y)


    print("Number of pbar events before oversampling: ", len(X[y == 1]))
    print("Number of cosmic events before oversampling: ", len(X[y == 0]))
    print(" ")
    print("Number of pbar events after oversampling: ", len(X_resampled[y_resampled == 1]))
    print("Number of cosmic events after oversampling: ", len(X_resampled[y_resampled == 0]))
    
    return X_resampled, y_resampled
    
#########################################################################################################  
#########################################################################################################  
    
def oversample_data(X, y, whichone, RANDOM):

    print("----------------------------------------------------------------------------") 
    print("Oversampling test...")
    sampler = None
    
    if whichone == "over_random":
        print("You chose random sampling.")
        sampler = RandomOverSampler(random_state=RANDOM)
        
    if whichone == "SMOTE":
        print("You chose SMOTE sampling")
        print("!!!!!!!! INFO !!!!!!!! There are additional choices! ('regular', 'borderline1', 'borderline2', 'svm')") 
        #print "standard setting: regular chosen"
        print("SVM test....") 
        #kind = ['regular', 'borderline1', 'borderline2', 'svm']
        sampler = SMOTE(kind = 'svm', random_state =RANDOM)

        
    X_resampled, y_resampled = sampler.fit_sample(X, y)


    print("Number of pbar events before oversampling: ", len(X[y == 1]))
    print("Number of cosmic events before oversampling: ", len(X[y == 0]))
    print(" ")
    print("Number of pbar events after oversampling: ", len(X_resampled[y_resampled == 1]))
    print("Number of cosmic events after oversampling: ", len(X_resampled[y_resampled == 0]))
    
       
    return X_resampled, y_resampled
    
    
#########################################################################################################    
#########################################################################################################     

def undersample_data(X, y, whichone, RANDOM):

    print("----------------------------------------------------------------------------") 
    print("Undersampling test...")
    sampler = None
    
    #X_cos_help = X[y==0]
    #y_cos_help = y[y==0]
    
    #X_help = X[y==1]
    #y_help = y[y==1]
    
    if whichone == "tomek":
        print("Tomek.........")
        sampler = TomekLinks(return_indices=False, random_state=RANDOM)
        
    if whichone == "under_random":
        print("random.........")
        sampler = RandomUnderSampler(return_indices=False, random_state=RANDOM)
        
    if whichone == "clustercentroids":
        print("cluster cetroids.........")
        sampler = ClusterCentroids(ratio = 'majority', voting = 'auto', random_state=RANDOM)
        
        
    X_resampled, y_resampled = sampler.fit_sample(X, y)


    print("Number of pbar events before undersampling: ", len(X[y == 1]))
    print("Number of cosmic events before undersampling: ", len(X[y == 0]))
    print(" ")
    print("Number of pbar events after undersampling: ", len(X_resampled[y_resampled == 1]))
    print("Number of cosmic events after undersampling: ", len(X_resampled[y_resampled == 0]))

    
    return X_resampled, y_resampled
    
#########################################################################################################  
#########################################################################################################  

def plot_E(E_before, E_after):
    print("HAI")

    fig, ax = plt.subplots(figsize = (15,8))

    ax.set_xlabel("BGO E deposit (MeV)")
    ax.set_ylabel("norm. counts")
    ax.set_yscale("log")
    ax.patch.set_facecolor='w'
    
    #ax.set_ylim(0.00001, 1)
    
    binnumber = 100
    range1 = 10
    range2 = 150        
   
    r = np.array([range1,range2])
    ax.set_xlim(range1,range2)


    histd, edgesd = np.histogram(a=E_after, bins=binnumber, range=r)    
    histd = histd.astype(np.float)
    normd = sum(np.diff(edgesd)*histd) # area
    bincentersd   = edgesd[:-1]+(edgesd[1]-edgesd[0])/2.

    histc, edgesc = np.histogram(a=E_before, bins=binnumber, range=r)    
    histc = histc.astype(np.float)
    normc = sum(np.diff(edgesc)*histc) # area
    bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.
   
    ax.errorbar(bincentersc, histc/normd, yerr = np.sqrt(histc)/normd, label = "Energy before sampling")#
    ax.errorbar(bincentersd, histc/normd, yerr = np.sqrt(histd)/normd, label = "Energy after sampling")#
        

    ax.legend(loc='upper right', fancybox=True, shadow=True,  prop={'size':13})   

    plt.savefig("test.png", facecolor=fig.get_facecolor())
    
    #plt.show()

#########################################################################################################  
#########################################################################################################  

def plot_Tracks(tracks_before, tracks_after):

    print("HAI")
    fig, ax = plt.subplots(figsize = (15,8))

    ax.set_xlabel("Number of tracks")
    ax.set_ylabel("norm. counts")
    #ax.set_yscale("log")
    ax.patch.set_facecolor='w'
        #ax.set_ylim(0.00001, 1)
    
    binnumber = 10
    range1 = 0.5
    range2 = 10.5        
    
    tracks_before = np.rint(tracks_before)
    tracks_after = np.rint(tracks_after)
   
    r = np.array([range1,range2])
    ax.set_xlim(range1,range2)

    histd, edgesd = np.histogram(a=tracks_after, bins=binnumber, range=r)    
    histd = histd.astype(np.float)
    normd = sum(np.diff(edgesd)*histd) # area
    bincentersd   = edgesd[:-1]+(edgesd[1]-edgesd[0])/2.

    histc, edgesc = np.histogram(a=tracks_before, bins=binnumber, range=r)    
    histc = histc.astype(np.float)
    normc = sum(np.diff(edgesc)*histc) # area
    bincentersc   = edgesc[:-1]+(edgesc[1]-edgesc[0])/2.
   
    #ax.errorbar(bincentersc, histc/normd, yerr = np.sqrt(histc)/normd, label = "Number of inner hodoscope hits, before sampling")#
    #ax.errorbar(bincentersd, histc/normd, yerr = np.sqrt(histd)/normd, label = "Number of inner hodoscope hits, after sampling")#
    leftc, rightc = edgesc[:-1],edgesc[1:]
    Xc = np.array([leftc,rightc]).T.flatten()
    Yc = np.array([histc/normc,histc/normc]).T.flatten()

    leftp, rightp = edgesd[:-1],edgesd[1:]
    Xp = np.array([leftp,rightp]).T.flatten()
    Yp = np.array([histd/normd,histd/normd]).T.flatten()

    plt.plot(Xc,Yc, label = "Number of tracks, before sampling")#
    plt.plot(Xp,Yp, label = "Number of tracks, after sampling")#

    ax.legend(loc='upper right', fancybox=True, shadow=True,  prop={'size':13})   

    plt.savefig("test.png", facecolor=fig.get_facecolor())
    
    plt.show()


#########################################################################################################  
#########################################################################################################  

def plot_Angle2(angle_before, angle_after):
    print("HAI")







def plot_Angle3(angle_before, angle_after):
    print("HAI")






def plot_ToF(tof_before, tof_after):
    print("HAI")


#"""

