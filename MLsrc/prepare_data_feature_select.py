from __future__ import print_function
from __future__ import absolute_import
#Import libraries:




########################################################################################################################
# this version of prepare data testing includes more features into the pdataframe, for the feature selection py script #
########################################################################################################################

import pandas as pd
import numpy as np
import math
#import xgboost as xgb
#from xgboost.sklearn import XGBClassifier   #xgboost module in python has an sklearn wrapper called XGBClassifier. It uses sklearn style naming convention.
#from sklearn import cross_validation, metrics   #Additional scklearn functions
#from sklearn.grid_search import GridSearchCV   #Perforing grid search

import matplotlib.pylab as plt
#%matplotlib inline
from matplotlib.pylab import rcParams

from .sampling import *
from .train_test_split import *

"""
0  file_mtds.write("%s " % cuspRunNumber)
1  file_mtds.write("%s " % midas_run_nr)
2  file_mtds.write("%s " % evtnumber)
3  file_mtds.write("%s " % BGO_E)    
4  file_mtds.write("%s " % nI_all)    
5  file_mtds.write("%s " % nO_all)                    
6  file_mtds.write("%s " % len(trackcollection))  
7  file_mtds.write("%s " % angle1)
8  file_mtds.write("%s " % angle2)
9  file_mtds.write("%s " % angle3)
10 file_mtds.write("%s " % mean_angleY)
11 file_mtds.write("%s " % mean_angleYm)
12 file_mtds.write("%s " % orientatio[0])
13 file_mtds.write("%s " % orientatio[1])
14 file_mtds.write("%s " % orientatio[2])               
15 file_mtds.write("%s " % tot_events)

16 file_mtds.write("%s " % mtd3)
17 file_mtds.write("%s " % mtd_min_max)

18 file_mtds.write("%s " % vertex[0])
19 file_mtds.write("%s " % vertex[1])

20 file_mtds.write("%s " % vertex2[0])
21 file_mtds.write("%s " % vertex2[1])
                    
22 file_mtds.write("%s " % nr_of_cluster)
23 file_mtds.write("%s " % cluster_dist)
24 file_mtds.write("%s " % number_of_noise_hits_I)
25 file_mtds.write("%s " % number_of_noise_hits_O)
26 file_mtds.write("%s " % number_of_orphans_I)
27 file_mtds.write("%s " % number_of_orphans_O)"""
##########################################################################################################################
def calc_ellipse(x, y, ellpar):

    center_x = ellpar[0]  
    center_y = ellpar[1]  
    Rx = ellpar[3]  
    Ry = ellpar[2]  
    angle = ellpar[4]/180*math.pi
    #angle = 30.0/180*math.pi

    #x_new = (x - center_x)*math.cos(angle) - (y - center_y)*math.sin(angle)
    #y_new = (y - center_y)*math.cos(angle) + (x - center_x)*math.sin(angle)
    dx = (x - center_x)
    dy = (y - center_y)
      
    condition = (dx*math.cos(angle) + dy*math.sin(angle))*(dx*math.cos(angle) + dy*math.sin(angle))/(Rx*Rx) + (dx*math.sin(angle) - dy*math.cos(angle))*(dx*math.sin(angle) - dy*math.cos(angle))/(Ry*Ry)
    
    if condition <= 1:
        return True
    #if dx*dx/(Rx*Rx) + dy*dy/(Ry*Ry) <= 1:    # outside of circle
    #    return True    
    else:
        return False
##########################################################################################################################
def cut_on_vertex(new_cands):
    events_cut = []
    events_out = []
    
    width = 45 # first vals
    height = 25 # first vals
    
    xy = [-23,-23]
    angle = -50
    #ellipse = Ellipse(xy= xy, width=width, height=height, angle=angle,edgecolor='r', fc='None', lw=2)   
    ell_par = [xy[0], xy[1], width*0.5, height*0.5, angle]
    
    for i in new_cands:
        if calc_ellipse(i[20], i[21], ell_par) == True: # 19 20 are x and y coords of vertex 
            #print i[9]
            #vertex_x.append(i[9])
            #vertex_y.append(i[10])
            events_cut.append(i)
        else:
        #    vertex_x.append(i[9])
        #    vertex_y.append(i[10])
            events_out.append(i)     
        
               
    events_cut = np.array(events_cut)
    events_out = np.array(events_out)


    return events_cut, events_out
##########################################################################################################################
def prepare_hbardata(hbardata_orig):
    hbardata = hbardata_orig[:,[3,4,5,6,7,8,9,10,14,16]] 
    
    hbardata = pd.DataFrame({'BGO_E':hbardata[:,0], 
                       'tracks':hbardata[:,1], 
                       'inner_hits':hbardata[:,2],
                       'outer_hits':hbardata[:,3],
                       'max_angle':hbardata[:,4],
                       'angle2':hbardata[:,5],
                       'angle3':hbardata[:,6],
                       'angle_Y':hbardata[:,7],
                       #'Nrh':hbardata[:,8],
                       'ToF':hbardata[:,9]
                       })
                       
                       
    #cols = ['inner_hits', 'outer_hits', 'tracks']
    #hbardata[cols] = hbardata[cols].applymap(np.int64)


    return hbardata
##########################################################################################################################
def prepare_cosmicdata(cosmicdata_orig):   
    cosmicdata = cosmicdata_orig[:,[3,4,5,6,7,8,9,10,14,16]] 
    
    cosmicdata = pd.DataFrame({'BGO_E':cosmicdata[:,0], 
                       'tracks':cosmicdata[:,1], 
                       'inner_hits':cosmicdata[:,2],
                       'outer_hits':cosmicdata[:,3],
                       'max_angle':cosmicdata[:,4],
                       'angle2':cosmicdata[:,5],
                       'angle3':cosmicdata[:,6],
                       'angle_Y':cosmicdata[:,7],
                       #'Nrh':cosmicdata[:,8],
                       'ToF':cosmicdata[:,9]
                       })
                       
    #cols = ['inner_hits', 'outer_hits', 'tracks']
    #cosmicdata[cols] = cosmicdata[cols].applymap(np.int64)

    return cosmicdata 
    

##########################################################################################################################

def prepare_pbardata(pbardata_orig):   
    bool_cut_vertex = True
    pbardata_out = None
    pbardata_out_orig = None
    
    pbardata_orig = pbardata_orig[pbardata_orig[:,28] > - 13]  # four seconds, see pbars_arriving.pdf
    pbardata_orig = pbardata_orig[pbardata_orig[:,28] < - 9]
    #print "pbar events after cut on timestamp: ", pbardata.shape
    pbardata_orig = pbardata_orig[pbardata_orig[:,22] <= 1] # no pbar hits with several hits!
    
    if bool_cut_vertex == True:
        pbardata_orig, pbardata_out_orig = cut_on_vertex(pbardata_orig)
        pbardata_out = pbardata_out_orig[:,[3,4,5,6,7,8,9,10,14,16]]    
        pbardata_out = pd.DataFrame({'BGO_E':pbardata_out[:,0], 
                       'tracks':pbardata_out[:,1], 
                       'inner_hits':pbardata_out[:,2],
                       'outer_hits':pbardata_out[:,3],
                       'max_angle':pbardata_out[:,4],
                       'angle2':pbardata_out[:,5],
                       'angle3':pbardata_out[:,6],
                       'angle_Y':pbardata_out[:,7],
                       #'Nrh':pbardata_out[:,8],
                       'ToF':pbardata_out[:,9]
                       })
    
    
    
    pbardata = pbardata_orig[:,[3,4,5,6,7,8,9,10,14,16]]    
    pbardata = pd.DataFrame({'BGO_E':pbardata[:,0], 
                       'tracks':pbardata[:,1], 
                       'inner_hits':pbardata[:,2],
                       'outer_hits':pbardata[:,3],
                       'max_angle':pbardata[:,4],
                       'angle2':pbardata[:,5],
                       'angle3':pbardata[:,6],
                       'angle_Y':pbardata[:,7],
                       #'Nrh':pbardata[:,8],
                       'ToF':pbardata[:,9]
                       })
                       
    #cols = ['inner_hits', 'outer_hits', 'tracks']
    #pbardata[cols] = pbardata[cols].applymap(np.int64)
    #pbardata_out[cols] = pbardata_out[cols].applymap(np.int64)

    return pbardata, pbardata_orig, pbardata_out, pbardata_out_orig
##########################################################################################################################
def prepare_data(cosmic_time, pbardata, cosmicdata, samp, test_size, seed, bool_cut_vertex, track_cut, validbool, valid_size, stratified_testing):
    # pbardata: pbar data set
    # cosmicdata: cosmics data set
    # samo: string, sampling method to use
    # test_size: fraction of data to use for test sample
    # seed: seed for random processes
    # bool_cut_vertex: cut on the vertex on the BGO
    # validbool: split of validation data sample 
    # valid_size: size of validation data sample
    # stratified_testing: enable stratified test sample 
    
    #tot_p = len(pbardata)  - estimated_cosmics
    #tot_c = len(cosmicdata)

    print("raw cosmic events: ", cosmicdata.shape)
    print("raw pbar events: ", pbardata.shape)

    pbardata = pbardata[pbardata[:,28] > - 13]  # for seconds, see pbars_arriving.pdf
    pbardata = pbardata[pbardata[:,28] < - 9] # column 27 is time after mixing signal (fake signal in case of pbar extraction)
    print("pbar events after cut on timestamp: ", pbardata.shape)
    pbardata = pbardata[pbardata[:,22] <= 1] # no pbar events with several hits on the BGO!
    
    len_before_track_cut = len(cosmicdata)    
    cosmicdata = cosmicdata[cosmicdata[:,6]>track_cut] # more than 1 track       
    len_after_track_cut = len(cosmicdata)    
    #exit(0)    
    
    if bool_cut_vertex == True:
        pbardata, pbardata_out = cut_on_vertex(pbardata)    
        print("pbar events after vertex cut: ", pbardata.shape)
        
        
    len_before_track_cut_pbar = len(pbardata)
    pbardata = pbardata[pbardata[:,6]>track_cut] # more than 1 track
    len_after_track_cut_pbar = len(pbardata)
    
    print(" cosmic len before and after track cut: ", len_before_track_cut, len_after_track_cut) 
    print(" pbar len before and after track cut: ", len_before_track_cut_pbar, len_after_track_cut_pbar)    
    
        
    print("!!!!!!!!!!!!!! INFO !!!!!!!!!!!!!! estimated cosmics will be scaled to pbar time and hit area.")
    
    pbar_time = 4*(102.0 - 9.0) # 4 seconds, 102 runs, 9 runs were empty! 
    area_hit = math.pi*45.0*0.5*25.0*0.5
    area_BGO = math.pi*45*45 #- area_hit
    
    estimated_cosmics = len(cosmicdata)*1.0/cosmic_time*pbar_time # scale to pbar time
    estimated_cosmics = estimated_cosmics*area_hit/area_BGO # scale to hit area
    print("estimated cosmics: ", estimated_cosmics)
    
    nr_of_features = 13
    
    pbardata = pbardata[:,[3,4,5,6,7,8,9,10,12,13,14,16,17]] #bgoE, tracks, angle1,angle2,angle (three larges angles, if only 2 tracks, angle,0,0)
                                                # mean angle with Y axis, nr of tracks horizontal, tof
    cosmicdata = cosmicdata[:,[3,4,5,6,7,8,9,10,12,13,14,16,17]] 
    #hbardata = hbardata_orig[:,[3,6,7,8,9,10,13,15]] 

    pbardata = np.hstack((pbardata, np.ones((pbardata.shape[0], 1), dtype=pbardata.dtype))) # add a column of '1's as Class label
    cosmicdata = np.hstack((cosmicdata, np.zeros((cosmicdata.shape[0], 1), dtype=cosmicdata.dtype))) # add a column of '0' as Class label 

    # for roots tmva the first event in the data matrix must be signal...
    tmva_hack_zero = (pbardata[0,:].reshape(1,len(pbardata[0,:])))[:,0:nr_of_features] 

    #print cosmicdata.shape
    #print pbardata.shape
    # add column 
 
    ################################################################################################################################# 
    print("-----------------------------------------------------------------------------------------------")
    
    if stratified_testing == False: 
    
        cosmic_test_ratio = test_size
    
        print("Splitting data into train and test data... test size: ", test_size, " -- not stratified!")    
    
        dataset = np.vstack((pbardata, cosmicdata)) # stack the two matrices
        dataset = np.take(dataset,np.random.permutation(dataset.shape[0]),axis=0,out=dataset) # mix it up 
         
        # X is a metrix with the features 
        X = dataset[:,0:nr_of_features] # all data noew 
        print(X.shape)
        # y is a vector with the CLass
        y = dataset[:,nr_of_features]
        print(y.shape)
        
        #test_size = 0.33   
        #seed = 7
        X_train, X_test, y_train, y_test = split_data_train_test(X, y, test_size, seed, estimated_cosmics)
        
    
    if stratified_testing == True:   
        
        print("Splitting data into train and test data... test size: ", test_size, " of pbar data -- splitting cosmics accordingly.")

        pbardata = np.hstack((pbardata, np.ones((pbardata.shape[0], 1), dtype=pbardata.dtype)))    
        X_train_p, X_test_p, y_train_p, y_test_p = train_test_split(pbardata[:,0:nr_of_features], pbardata[:,nr_of_features], test_size=test_size, random_state=seed)
        
        #print "X train p", len(X_train_p), len(y_train_p) 
        #print "y train p", len(X_test_p), len(y_test_p)
           
        cosmicdata = np.hstack((cosmicdata, np.zeros((cosmicdata.shape[0], 1), dtype=cosmicdata.dtype)))    
        
        cosmic_test_ratio = len(X_test_p)/(1.0*len(cosmicdata[:,0:nr_of_features]))
        
        X_train_c, X_test_c, y_train_c, y_test_c = train_test_split(cosmicdata[:,0:nr_of_features], cosmicdata[:,nr_of_features], test_size=cosmic_test_ratio, random_state=seed)
            
        #print "X train c", len(X_train_c), len(y_train_c)
        #print "y train c", len(X_test_c), len(y_test_c)
        
        tmva_hack_zero = (pbardata[0,:].reshape(1,len(pbardata[0,:])))[:,0:nr_of_features]

        dataset = np.vstack((np.hstack((X_train_p, y_train_p.reshape(len(y_train_p),1))), np.hstack((X_train_c, y_train_c.reshape(len(y_train_c),1)))))    
        dataset = np.take(dataset,np.random.permutation(dataset.shape[0]),axis=0,out=dataset)
            
        X_train = dataset[:,0:nr_of_features]   
        #print X_train.shape
        y_train = dataset[:,nr_of_features]
        #print y_train.shape
        
        dataset = np.vstack((np.hstack((X_test_p, y_test_p.reshape(len(y_test_p),1))), np.hstack((X_test_c, y_test_c.reshape(len(y_test_c),1)))))     
        dataset = np.take(dataset,np.random.permutation(dataset.shape[0]),axis=0,out=dataset)
        
        X_test = dataset[:,0:nr_of_features]  
        #print X_train.shape
        y_test = dataset[:,nr_of_features]
        #print y_test.shape
    
    
    print("pbar train data size: ", len(X_train[y_train == 1]))
    print("cosmic train data size: ", len(X_train[y_train == 0]))
    
    print("pbar test data size: ", len(X_test[y_test == 1]))
    print("cosmic test data size: ", len(X_test[y_test == 0]))   
    
    print(" ")
    
    estimated_cosmics_test = estimated_cosmics*cosmic_test_ratio
    tot_p_test = len(X_test[y_test == 1]) - estimated_cosmics_test 
    
    print("estimated cosmics in test sample (scaled with test_size ",test_size,"): ", estimated_cosmics_test)
    print("estimated tot pbars in test sample: ", tot_p_test)
    
    print("-----------------------------------------------------------------------------------------------")

    
    #################################################################################################################################    
    
    
    if validbool == True:
    
        #X_train, X_valid, y_train, y_valid = split_data_train_test(X_train, y_train, valid_size, seed, estimated_cosmics)
                  
        X_train_p, X_valid_p, y_train_p, y_valid_p = train_test_split(X_train[y_train == 1], y_train[y_train == 1], test_size=valid_size, random_state=seed)  
        
        cosmic_valid_ratio = len(X_valid_p)/(1.0*len(X_train[y_train==0]))
        
        X_train_c, X_valid_c, y_train_c, y_valid_c = train_test_split(X_train[y_train == 0], y_train[y_train == 0], test_size=cosmic_valid_ratio, random_state=seed)
                    
        tmva_hack_zero = (pbardata[0,:].reshape(1,len(pbardata[0,:])))[:,0:nr_of_features]

        dataset = np.vstack((np.hstack((X_train_p, y_train_p.reshape(len(y_train_p),1))), np.hstack((X_train_c, y_train_c.reshape(len(y_train_c),1)))))    
        dataset = np.take(dataset,np.random.permutation(dataset.shape[0]),axis=0,out=dataset)
            
        X_train = dataset[:,0:nr_of_features]   
        print(X_train.shape)
        y_train = dataset[:,nr_of_features]
        print(y_train.shape)
        
        dataset = np.vstack((np.hstack((X_valid_p, y_valid_p.reshape(len(y_valid_p),1))), np.hstack((X_valid_c, y_valid_c.reshape(len(y_valid_c),1)))))     
        dataset = np.take(dataset,np.random.permutation(dataset.shape[0]),axis=0,out=dataset)
        
        X_valid = dataset[:,0:nr_of_features]  
        print(X_valid.shape)
        y_valid = dataset[:,nr_of_features]
        print(y_valid.shape)
    
    #######################################################################################################################################################

    if samp in ["SMOTE", "over_random"]:
        X_resampled, y_resampled = oversample_data(X_train, y_train, samp, 6)  
        
    elif samp in ["SMOTEENN", "SMOTETomek"]:
        print("HERE!")
        X_resampled, y_resampled = combinedsample_data(X_train, y_train, samp, 6)  
        
    elif samp in ["tomek", "under_random", "clustercentroids"]:
        X_resampled, y_resampled = undersample_data(X_train, y_train, samp, 6)

    elif samp == "none":
        X_resampled = X_train
        y_resampled = y_train  
        
            
    # plot distributions before/after undersampling/oversampling
    #plot_E((X[y == 1])[:,0], (X_resampled[y_resampled == 1])[:,0])
    
    #plot_Tracks((X_train[y_train == 1])[:,1], (Xtot_resampled[ytot_resampled == 1])[:,1])

    X_train = X_resampled
    y_train = y_resampled
    
    #print "Xtot ", X_tot
    #print "ytot ", y_tot
    
    
    ##############################################################
    # this is needed for TMVA - the first entry (event) in the matrix needs to be signal (class 1).... 
    #print "shape", X_train.shape
    #print "shape", tmva_hack_zero.shape    
    X_train_n = np.vstack((tmva_hack_zero, X_train))
    
    y_train_n = np.insert(y_train,0,1)
    
    #print "shape", y_train_n.shape
    ###############################################################
    
    data = np.append(X_train_n, y_train_n.reshape(len(y_train_n), 1), axis=1)  
    #data = np.append(X_test, y_test.reshape(len(y_test), 1), axis=1)   
    # set max_angle, angle2 and angle3 to 999 instead of 0, so we can use the missing feature option of XGBoost
    mask = data[:, 4] == 0
    data[:, 4][mask] = 999
    mask = data[:, 5] == 0
    data[:, 5][mask] = 999
    mask = data[:, 6] == 0
    data[:, 6][mask] = 999

    mask = data[:, 8] == 999
    data[:, 8][mask] = 0
    mask = data[:, 9] == 999
    data[:, 9][mask] = 0
    mask = data[:, 10] == 999
    data[:, 10][mask] = 0    

    mask = data[:, 11] == 0
    data[:, 11][mask] = 999

    mask = data[:, 12] == 0
    data[:, 12][mask] = 999
    
    train = pd.DataFrame({'BGO_E':data[:,0],                        
                       'inner_hits':np.rint(data[:,1]),
                       'outer_hits':np.rint(data[:,2]),
                       'tracks':np.rint(data[:,3]), 
                       'max_angle':data[:,4],
                       'angle2':data[:,5],
                       'angle3':data[:,6],
                       'angle_Y':data[:,7],
                       'Nru':np.rint(data[:,8]),
                       'Nrd':np.rint(data[:,9]),
                       'Nrh':np.rint(data[:,10]),
                       'ToF':data[:,11],
                       'ToF2':data[:,12],
                       'Class':data[:,nr_of_features]     # can be 1 or 0
                       })
                       
    #cols = ['inner_hits', 'outer_hits', 'tracks']
    #train[cols] = train[cols].applymap(np.int64)
     
    data = np.append(X_test, y_test.reshape(len(y_test), 1), axis=1)      
    mask = data[:, 4] == 0
    data[:, 4][mask] = 999
    mask = data[:, 5] == 0
    data[:, 5][mask] = 999
    mask = data[:, 6] == 0
    data[:, 6][mask] = 999  

    mask = data[:, 8] == 999
    data[:, 8][mask] = 0
    mask = data[:, 9] == 999
    data[:, 9][mask] = 0
    mask = data[:, 10] == 999
    data[:, 10][mask] = 0    

    mask = data[:, 11] == 0
    data[:, 11][mask] = 999

    mask = data[:, 12] == 0
    data[:, 12][mask] = 999
                       
    test = pd.DataFrame({'BGO_E':data[:,0], 
                       'inner_hits':np.rint(data[:,1]),
                       'outer_hits':np.rint(data[:,2]),
                       'tracks':np.rint(data[:,3]),
                       'max_angle':data[:,4],
                       'angle2':data[:,5],
                       'angle3':data[:,6],
                       'angle_Y':data[:,7],
                       'Nru':np.rint(data[:,8]),
                       'Nrd':np.rint(data[:,9]),
                       'Nrh':np.rint(data[:,10]),
                       'ToF':data[:,11],
                       'ToF2':data[:,12],
                       'Class':data[:,nr_of_features]    # can be 1 or 0
                       })
                       
         
    if validbool == True:                   
        data = np.append(X_valid, y_valid.reshape(len(y_valid), 1), axis=1)      
        mask = data[:, 4] == 0
        data[:, 4][mask] = 999
        mask = data[:, 5] == 0
        data[:, 5][mask] = 999
        mask = data[:, 6] == 0
        data[:, 6][mask] = 999        

        mask = data[:, 8] == 999
        data[:, 8][mask] = 0
        mask = data[:, 9] == 999
        data[:, 9][mask] = 0
        mask = data[:, 10] == 999
        data[:, 10][mask] = 0    

        mask = data[:, 11] == 0
        data[:, 11][mask] = 999

        mask = data[:, 12] == 0
        data[:, 12][mask] = 999          
                           

        valid = pd.DataFrame({'BGO_E':data[:,0],
                       'inner_hits':np.rint(data[:,1]),
                       'outer_hits':np.rint(data[:,2]),
                       'tracks':np.rint(data[:,3]),                       
                       'max_angle':data[:,4],
                       'angle2':data[:,5],
                       'angle3':data[:,6],
                       'angle_Y':data[:,7],
                       'Nru':np.rint(data[:,8]),
                       'Nrd':np.rint(data[:,9]),
                       'Nrh':np.rint(data[:,10]),
                       'ToF':data[:,11],
                       'ToF2':data[:,12],
                       'Class':data[:,nr_of_features] 
                       })
                       

        return train, test, valid, estimated_cosmics, cosmic_test_ratio
    else:    
        return train, test, estimated_cosmics, cosmic_test_ratio
    
    
    
    
    
    
    
    
    
    
