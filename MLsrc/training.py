from __future__ import print_function
# First XGBoost model for Pima Indians dataset
from numpy import loadtxt
from xgboost import XGBClassifier
import xgboost as xgb
from sklearn import neighbors, datasets
from sklearn.metrics import accuracy_score
from sys import argv
import matplotlib.pyplot as plt
import numpy as np

import sklearn

##############################################################################################
##############################################################################################
def XGBoost_train2(X_train, y_train, X_test, y_test): 
  
    dtrain = xgb.DMatrix(X_train, label=y_train)
    dtest = xgb.DMatrix(X_test, label=y_test)    
    
    param = {'bst:max_depth':2, 'bst:eta':1, 'silent':1, 'objective':'binary:logistic' }
    plst = param.items()

    bst = xgb.train(plst,dtrain,)
    
    ypred = bst.predict(dtest)

    #print ypred
    return ypred
    
    
 ##############################################################################################
##############################################################################################
def XGBoost_train(X_train, y_train): 

    # fit model no training data
    model = XGBClassifier()
    model.fit(X_train, y_train)
    
            
    return model
##############################################################################################
##############################################################################################
    
def kNN_train(X_train, y_train, nrN, hsize): 
    print("----------------------------------------------------------------------------") 
    print("------------------------           kNN           ---------------------------") 
    print("----------------------------------------------------------------------------") 
       
    n_neighbors = nrN # neighbours in the box for estimation of pdfs around point
    h = hsize # step size in the mesh
      
    print("!!!!!!!! INFO !!!!!!!! More choices! Weights is set to uniform atm.")  
    print("!!!!!!!! INFO !!!!!!!! Even more choices! algorithm is set to ball_tree atm.")  
    
    weights = 'uniform'
    #weights = 'distance'

    # we create an instance of Neighbours Classifier and fit the data.
    model = neighbors.KNeighborsClassifier(n_neighbors, weights=weights, algorithm = 'ball_tree')

    #print "start training"
    model.fit(X_train, y_train)
    #print "end training"
    
    return model
