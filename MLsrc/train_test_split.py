from __future__ import print_function
# First XGBoost model for Pima Indians dataset
from numpy import loadtxt

from sklearn.metrics import accuracy_score
from sys import argv
import matplotlib.pyplot as plt
import numpy as np

import sklearn
from sklearn.model_selection import train_test_split

def split_data_train_test(X, y, test_size, seed, estimated_cosmics):

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=seed)

    print("----------------------------------------------------------------------------")  
    print("Splitting data into train and test data... test size: ", test_size)
    print("pbar train data size: ", len(X_train[y_train == 1]))
    print("cosmic train data size: ", len(X_train[y_train == 0]))
    
    print("pbar test data size: ", len(X_test[y_test == 1]))
    print("cosmic test data size: ", len(X_test[y_test == 0]))   
    
    print(" ")
    
    estimated_cosmics_test = estimated_cosmics*test_size
    tot_p_test = len(X_test[y_test == 1]) - estimated_cosmics_test 
    
    print("estimated cosmics in test sample (scaled with test_size ",test_size,"): ", estimated_cosmics_test)
    print("estimated tot pbars in test sample: ", tot_p_test)
    

    print("----------------------------------------------------------------------------") 


    return X_train, X_test, y_train, y_test
