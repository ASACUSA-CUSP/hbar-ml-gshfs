from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn import neighbors, datasets
import sklearn
from sys import argv
import numpy as np

####################################################################################################################################################
if __name__ == '__main__':

    xgboost_runs = int(argv[2])
    
    evs = len(np.loadtxt(argv[1])[:,0])
    mean_score_arr = np.zeros((xgboost_runs, evs))
    
    FI = np.array([-5000, -400, -70, -40])
    cands_arr = []
    
    print(mean_score_arr.shape)

    for i in range(xgboost_runs): # loop over all hbar_cands files
        name = "./hbar_xgboost_cands_rounds/hbars_xgboost_round_" + str(i) + "_FI.dat"
        compare_cands = np.loadtxt(name)
        
        """# count_cands = compare_cands[np.logical_and(compare_cands[:,30] > 1, compare_cands[:,31] > 1)]
        # 
        #countcands_5000 = count_cands[count_cands[:,33] == -5000]
        countcands_400 = count_cands[count_cands[:,33] == -400]
        countcands_70 = count_cands[count_cands[:,33] == -70]
        countcands_40 = count_cands[count_cands[:,33] == -40]
        
        countcands_5000 = len(countcands_5000[countcands_5000[:,0]>0.395])
        countcands_400 = len(countcands_400[countcands_400[:,0]>0.395])
        countcands_70 = len(countcands_70[countcands_70[:,0]>0.395])
        countcands_40 = len(countcands_40[countcands_40[:,0]>0.395])#"""
        
        
        
        
        #cands_arr.append([countcands_5000, countcands_400, countcands_70, countcands_40])
        
        scores = compare_cands[:,0]
        
        #print i
        #print scores
        mean_score_arr[i] = scores
        
    """cands_arr = np.array(cands_arr)
    np.savetxt("number_of_cands_0395.dat", cands_arr, delimiter = ' ')
    
    print cands_arr    
    print np.mean(cands_arr), np.std(cands_arr)
    
    exit(0)"""
    ##############################################################    
    
    print(mean_score_arr.shape)
    
    mean_score_arr_final = np.mean(mean_score_arr, axis = 0)
    std_score_arr_final = np.std(mean_score_arr, axis = 0)
    median_score_arr_final = np.median(mean_score_arr, axis = 0)
    ##############################################################
    
    evnts = np.loadtxt(argv[1])
    print(evnts.shape)
    
    print(std_score_arr_final.shape)
    std_score_arr_final.shape = (len(std_score_arr_final), 1)
    mean_score_arr_final.shape = (len(mean_score_arr_final), 1)
    
    print(std_score_arr_final.shape)
    
    evnts = np.hstack((std_score_arr_final, mean_score_arr_final, evnts))
    
    print(np.max(mean_score_arr_final))
    
    
    evnts[:,2] = median_score_arr_final    
    #print (np.mean(mean_score_arr, axis = 0)).shape

    np.savetxt('xgb_events_cands_mean_score.dat', evnts, delimiter = " ") 
    
   
